module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'comma-dangle': [2, 'always-multiline'],
    'space-before-function-paren': 'off',
    // GraphQLの対象はスネークケースが多い
    '@typescript-eslint/camelcase': 'off',
    // InterfaceをI始まりで定義できるようにoff
    '@typescript-eslint/interface-name-prefix': 'off',
  },
}
