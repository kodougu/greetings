import { logger } from './logger'

export async function playSound(audio: string) {
  return new Promise<void>((resolve, reject) => {
    try {
      const myAudio = new Audio(audio)
      myAudio.volume = 0.1
      myAudio.addEventListener('ended', function() {
        myAudio.currentTime = 0
        logger.trace('ended')
        return resolve()
      })
      myAudio.play().catch(e => {
        logger.error(e)
        return reject(e)
      })
    } catch (e) {
      logger.error(e)
      return resolve()
    }
  })
}
