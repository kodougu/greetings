export function truncate(
  value: string,
  length: number | string = 20,
  omission = '...',
): string {
  if (typeof length === 'string') {
    length = parseInt(length, 10)
  }
  if (value.length <= length) {
    return value
  }
  return value.substring(0, length) + omission
}
