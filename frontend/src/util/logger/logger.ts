/* eslint import/first: 0 */
import dotenv from 'dotenv'
dotenv.config()

import { LEVEL } from '@/util/logger/logLevel'

function getLevel(level: string | undefined): LEVEL {
  if (level === 'TRACE') return 'TRACE'
  if (level === 'DEBUG') return 'DEBUG'
  if (level === 'INFO') return 'INFO'
  if (level === 'WARN') return 'WARN'
  if (level === 'ERROR') return 'ERROR'
  if (level === 'FATAL') return 'FATAL'
  return 'WARN'
}

export abstract class Logger {
  dateTimeFormat = 'YYYY-MM-DDTHH:mm:ss.SSS.Z'
  category = ''

  constructor(category: string) {
    this.category = category
    const logLevelEnvKey = 'VUE_APP_LOG_LEVEL_' + category.toUpperCase()
    let level: LEVEL = 'INFO'
    if (process && process.env) {
      console.log(process.env)
      if (process.env[logLevelEnvKey]) {
        level = getLevel(process.env[logLevelEnvKey])
      } else if (process.env.NODE_ENV) {
        const mode = process.env.NODE_ENV
        if (mode === 'development') {
          level = 'INFO'
        } else if (mode === 'test') {
          level = 'WARN'
        } else if (mode === 'production') {
          level = 'WARN'
        } else {
          level = 'WARN'
        }
      }
    }
    console.log(`LOG: ${category} LEVEL: ${level} logLevelEnvKey: ${logLevelEnvKey}`)
    this.setLevel(level)
  }

  abstract setLevel(level: LEVEL): void
  // eslint-disable-next-line
  abstract enhance(target: any): any

  log(target: any) { } // eslint-disable-line
  trace(target: any) { } // eslint-disable-line
  debug(target: any) { } // eslint-disable-line
  info(target: any) { } // eslint-disable-line
  warn(target: any) { } // eslint-disable-line
  error(target: any) { } // eslint-disable-line
  fatal(target: any) { } // eslint-disable-line
  t(title: string, target: any) { } // eslint-disable-line
  d(title: string, target: any) { } // eslint-disable-line
  i(title: string, target: any) { } // eslint-disable-line
  w(title: string, target: any) { } // eslint-disable-line
  e(title: string, target: any) { } // eslint-disable-line
  f(title: string, target: any) { } // eslint-disable-line

  isTraceEnable(): boolean {
    return false
  }

  isDebugEnable(): boolean {
    return false
  }

  isInfoEnable(): boolean {
    return false
  }

  isWarnEnable(): boolean {
    return false
  }

  isErrorEnable(): boolean {
    return false
  }

  isFatalEnable(): boolean {
    return false
  }
}
