import { BrowserLogger } from '@/util/logger/browserLogger'
import { Logger } from '@/util/logger/logger'

function newInstance(category: string): Logger {
  return new BrowserLogger(category)
}

export const logger: Logger = newInstance('default')

const loggerMap = new Map<string, Logger>()
loggerMap.set('default', logger)

export function getLogger(category: string): Logger {
  if (loggerMap.has(category)) {
    const l = loggerMap.get(category)
    return l || logger
  } else {
    return newInstance(category)
  }
}

// eslint-disable-next-line
export function enhance(target: any): any {
  return logger.enhance(target)
}
