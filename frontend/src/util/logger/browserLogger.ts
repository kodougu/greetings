import { Logger } from '@/util/logger/logger'
import { LEVEL } from '@/util/logger/logLevel'
import { isObject } from '../is'
import { stringify } from '../stringify'

const TRACE = 'color:gray;font-weight:normal;'
const DEBUG = 'color:lightgreen;font-weight:normal;'
const INFO = 'color:skyblue;font-weight:normal;'
const WARN = 'color:orange;font-weight:bold;'
const ERROR = 'color:red;font-weight:bold;'
const FATAL = 'color:darkviolet;font-weight:bold;'

const LINE_FORMAT = '%c%s%s'
const DETAIL_FORMAT = '%c%s'

export class BrowserLogger extends Logger {
  // eslint-disable-next-line
  enhance(target: any): any {
    let msg = ''
    if (target instanceof Error) {
      msg += target.stack
    } else if (isObject(target)) {
      msg += stringify(target)
    } else {
      if (target !== undefined) {
        msg += target
      }
    }
    return msg
  }

  /* eslint-disable no-console */
  // eslint-disable-next-line
  private groupOutput(title: string, target: any, level: LEVEL) {
    switch (level) {
      case 'TRACE':
        console.groupCollapsed(LINE_FORMAT, TRACE, '[TRACE]', title)
        console.log(DETAIL_FORMAT, TRACE, target)
        break
      case 'DEBUG':
        console.groupCollapsed(LINE_FORMAT, DEBUG, '[DEBUG]', title)
        console.log(DETAIL_FORMAT, DEBUG, target)
        break
      case 'INFO':
        console.groupCollapsed(LINE_FORMAT, INFO, '[INFO]', title)
        console.info(DETAIL_FORMAT, INFO, target)
        break
      case 'WARN':
        console.groupCollapsed(LINE_FORMAT, WARN, '[WARN]', title)
        console.warn(DETAIL_FORMAT, WARN, target)
        break
      case 'ERROR':
        console.groupCollapsed(LINE_FORMAT, ERROR, '[ERROR]', title)
        console.error(DETAIL_FORMAT, ERROR, target)
        break
      case 'FATAL':
        console.groupCollapsed(LINE_FORMAT, FATAL, '[FATAL]', title)
        console.error(DETAIL_FORMAT, FATAL, target)
        break
    }
    console.groupEnd()
  }

  setLevel(level: LEVEL) {
    switch (level) {
      case 'TRACE':
        this.trace = console.log.bind(console, LINE_FORMAT, TRACE, '[TRACE]')
        this.log = console.log.bind(console, LINE_FORMAT, TRACE)
        // eslint-disable-next-line
        this.t = (title: string, target: any) => {
          this.groupOutput(title, target, 'TRACE')
        }
        this.isTraceEnable = () => {
          return true
        }
      /* falls through */
      case 'DEBUG':
        this.debug = console.log.bind(console, LINE_FORMAT, DEBUG, '[DEBUG]')
        // eslint-disable-next-line
        this.d = (title: string, target: any) => {
          this.groupOutput(title, target, 'DEBUG')
        }
        this.isDebugEnable = () => {
          return true
        }
      /* falls through */
      case 'INFO':
        this.info = console.info.bind(console, LINE_FORMAT, INFO, '[INFO]')
        // eslint-disable-next-line
        this.i = (title: string, target: any) => {
          this.groupOutput(title, target, 'INFO')
        }
        this.isInfoEnable = () => {
          return true
        }
      /* falls through */
      case 'WARN':
        this.warn = console.warn.bind(console, LINE_FORMAT, WARN, '[WARN]')
        // eslint-disable-next-line
        this.w = (title: string, target: any) => {
          this.groupOutput(title, target, 'WARN')
        }
        this.isWarnEnable = () => {
          return true
        }
      /* falls through */
      case 'ERROR':
        this.error = console.error.bind(console, LINE_FORMAT, ERROR, '[ERROR]')
        // eslint-disable-next-line
        this.e = (title: string, target: any) => {
          this.groupOutput(title, target, 'ERROR')
        }
        this.isErrorEnable = () => {
          return true
        }
      /* falls through */
      case 'FATAL':
        this.fatal = console.error.bind(console, LINE_FORMAT, FATAL, '[FATAL]')
        // eslint-disable-next-line
        this.f = (title: string, target: any) => {
          this.groupOutput(title, target, 'FATAL')
        }
        this.isFatalEnable = () => {
          return true
        }
    }
  }
  /* eslint-enable no-console */
}
