export async function wait(term = 100) {
  return new Promise<void>(resolve => {
    setTimeout(() => {
      return resolve()
    }, term)
  })
}
