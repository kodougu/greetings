export function randomInteger(min: number, max: number): number {
  return Math.floor(Math.random() * (max + 1 - min)) + min
}
export function randomElements<T>(arr: Array<T>, count = 1): Array<T> {
  const results = []
  const addedIndex: number[] = []
  while (results.length <= count && results.length < arr.length) {
    const index = randomInteger(0, arr.length - 1)
    if (!addedIndex.includes(index)) {
      addedIndex.push(index)
      results.push(arr[index])
    }
  }
  return results
}
export function randomElement<T>(arr: Array<T>): T {
  return arr[randomInteger(0, arr.length - 1)]
}
export function randomString(length?: number, seed?: string): string {
  const SEED = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
  const LENGTH = 4
  if (!seed) seed = SEED
  if (!length) length = LENGTH
  // ランダムな文字列の生成
  let result = ''
  for (let i = 0; i < length; i++) {
    result += seed.charAt(Math.floor(Math.random() * seed.length))
  }
  return result
}
