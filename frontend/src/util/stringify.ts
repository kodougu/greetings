// eslint-disable-next-line
export function decycle(object: any) {
  const objects = new WeakMap()
  return (function derez(value, path) {
    // eslint-disable-next-line
    let oldPath: any
    // eslint-disable-next-line
    let nu: any
    if (
      typeof value === 'object' &&
      value !== null &&
      !(value instanceof Boolean) &&
      !(value instanceof Date) &&
      !(value instanceof Number) &&
      !(value instanceof RegExp) &&
      !(value instanceof String)
    ) {
      oldPath = objects.get(value)
      if (oldPath !== undefined) {
        return { $ref: oldPath }
      }
      objects.set(value, path)
      if (Array.isArray(value)) {
        nu = []
        value.forEach(function(element, i) {
          nu[i] = derez(element, `${path}[${i}]`)
        })
      } else {
        nu = {}
        Object.keys(value).forEach(function(name) {
          nu[name] = derez(value[name], `${path}[${JSON.stringify(name)}]`)
        })
      }
      return nu
    }
    return value
  })(object, '$')
}

// eslint-disable-next-line
export function stringify(object: any, space?: string | number) {
  return JSON.stringify(decycle(object), null, space)
}
