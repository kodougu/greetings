import { GraphQLClient } from 'graphql-request';
import { print } from 'graphql';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  timestamp: any;
  uuid: any;
};

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>;
  _gt?: Maybe<Scalars['Int']>;
  _gte?: Maybe<Scalars['Int']>;
  _in?: Maybe<Array<Scalars['Int']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['Int']>;
  _lte?: Maybe<Scalars['Int']>;
  _neq?: Maybe<Scalars['Int']>;
  _nin?: Maybe<Array<Scalars['Int']>>;
};

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>;
  _gt?: Maybe<Scalars['String']>;
  _gte?: Maybe<Scalars['String']>;
  _ilike?: Maybe<Scalars['String']>;
  _in?: Maybe<Array<Scalars['String']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _like?: Maybe<Scalars['String']>;
  _lt?: Maybe<Scalars['String']>;
  _lte?: Maybe<Scalars['String']>;
  _neq?: Maybe<Scalars['String']>;
  _nilike?: Maybe<Scalars['String']>;
  _nin?: Maybe<Array<Scalars['String']>>;
  _nlike?: Maybe<Scalars['String']>;
  _nsimilar?: Maybe<Scalars['String']>;
  _similar?: Maybe<Scalars['String']>;
};

/**
 * アカウント: account
 *
 *
 * columns and relationships of "account"
 */
export type Account = {
  __typename?: 'account';
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** id */
  id: Scalars['uuid'];
  /** nickname */
  nickname?: Maybe<Scalars['String']>;
  /** An array relationship */
  participants: Array<Participant>;
  /** An aggregated array relationship */
  participants_aggregate: Participant_Aggregate;
  /** An array relationship */
  talks: Array<Talk>;
  /** An aggregated array relationship */
  talks_aggregate: Talk_Aggregate;
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};


/**
 * アカウント: account
 *
 *
 * columns and relationships of "account"
 */
export type AccountParticipantsArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/**
 * アカウント: account
 *
 *
 * columns and relationships of "account"
 */
export type AccountParticipants_AggregateArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/**
 * アカウント: account
 *
 *
 * columns and relationships of "account"
 */
export type AccountTalksArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/**
 * アカウント: account
 *
 *
 * columns and relationships of "account"
 */
export type AccountTalks_AggregateArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};

/** aggregated selection of "account" */
export type Account_Aggregate = {
  __typename?: 'account_aggregate';
  aggregate?: Maybe<Account_Aggregate_Fields>;
  nodes: Array<Account>;
};

/** aggregate fields of "account" */
export type Account_Aggregate_Fields = {
  __typename?: 'account_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Account_Max_Fields>;
  min?: Maybe<Account_Min_Fields>;
};


/** aggregate fields of "account" */
export type Account_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Account_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "account" */
export type Account_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Account_Max_Order_By>;
  min?: Maybe<Account_Min_Order_By>;
};

/** input type for inserting array relation for remote table "account" */
export type Account_Arr_Rel_Insert_Input = {
  data: Array<Account_Insert_Input>;
  on_conflict?: Maybe<Account_On_Conflict>;
};

/** Boolean expression to filter rows from the table "account". All fields are combined with a logical 'AND'. */
export type Account_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Account_Bool_Exp>>>;
  _not?: Maybe<Account_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Account_Bool_Exp>>>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  nickname?: Maybe<String_Comparison_Exp>;
  participants?: Maybe<Participant_Bool_Exp>;
  talks?: Maybe<Talk_Bool_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "account" */
export enum Account_Constraint {
  /** unique or primary key constraint */
  AccountPkc = 'account_pkc'
}

/** input type for inserting data into table "account" */
export type Account_Insert_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  nickname?: Maybe<Scalars['String']>;
  participants?: Maybe<Participant_Arr_Rel_Insert_Input>;
  talks?: Maybe<Talk_Arr_Rel_Insert_Input>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Account_Max_Fields = {
  __typename?: 'account_max_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  nickname?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "account" */
export type Account_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Account_Min_Fields = {
  __typename?: 'account_min_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  nickname?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "account" */
export type Account_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "account" */
export type Account_Mutation_Response = {
  __typename?: 'account_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Account>;
};

/** input type for inserting object relation for remote table "account" */
export type Account_Obj_Rel_Insert_Input = {
  data: Account_Insert_Input;
  on_conflict?: Maybe<Account_On_Conflict>;
};

/** on conflict condition type for table "account" */
export type Account_On_Conflict = {
  constraint: Account_Constraint;
  update_columns: Array<Account_Update_Column>;
  where?: Maybe<Account_Bool_Exp>;
};

/** ordering options when selecting data from "account" */
export type Account_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  nickname?: Maybe<Order_By>;
  participants_aggregate?: Maybe<Participant_Aggregate_Order_By>;
  talks_aggregate?: Maybe<Talk_Aggregate_Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "account" */
export type Account_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "account" */
export enum Account_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Nickname = 'nickname',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "account" */
export type Account_Set_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  nickname?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** update columns of table "account" */
export enum Account_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Nickname = 'nickname',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root';
  /** delete data from the table: "account" */
  delete_account?: Maybe<Account_Mutation_Response>;
  /** delete single row from the table: "account" */
  delete_account_by_pk?: Maybe<Account>;
  /** delete data from the table: "participant" */
  delete_participant?: Maybe<Participant_Mutation_Response>;
  /** delete single row from the table: "participant" */
  delete_participant_by_pk?: Maybe<Participant>;
  /** delete data from the table: "room" */
  delete_room?: Maybe<Room_Mutation_Response>;
  /** delete single row from the table: "room" */
  delete_room_by_pk?: Maybe<Room>;
  /** delete data from the table: "room_topic" */
  delete_room_topic?: Maybe<Room_Topic_Mutation_Response>;
  /** delete single row from the table: "room_topic" */
  delete_room_topic_by_pk?: Maybe<Room_Topic>;
  /** delete data from the table: "talk" */
  delete_talk?: Maybe<Talk_Mutation_Response>;
  /** delete single row from the table: "talk" */
  delete_talk_by_pk?: Maybe<Talk>;
  /** delete data from the table: "topic" */
  delete_topic?: Maybe<Topic_Mutation_Response>;
  /** delete single row from the table: "topic" */
  delete_topic_by_pk?: Maybe<Topic>;
  /** insert data into the table: "account" */
  insert_account?: Maybe<Account_Mutation_Response>;
  /** insert a single row into the table: "account" */
  insert_account_one?: Maybe<Account>;
  /** insert data into the table: "participant" */
  insert_participant?: Maybe<Participant_Mutation_Response>;
  /** insert a single row into the table: "participant" */
  insert_participant_one?: Maybe<Participant>;
  /** insert data into the table: "room" */
  insert_room?: Maybe<Room_Mutation_Response>;
  /** insert a single row into the table: "room" */
  insert_room_one?: Maybe<Room>;
  /** insert data into the table: "room_topic" */
  insert_room_topic?: Maybe<Room_Topic_Mutation_Response>;
  /** insert a single row into the table: "room_topic" */
  insert_room_topic_one?: Maybe<Room_Topic>;
  /** insert data into the table: "talk" */
  insert_talk?: Maybe<Talk_Mutation_Response>;
  /** insert a single row into the table: "talk" */
  insert_talk_one?: Maybe<Talk>;
  /** insert data into the table: "topic" */
  insert_topic?: Maybe<Topic_Mutation_Response>;
  /** insert a single row into the table: "topic" */
  insert_topic_one?: Maybe<Topic>;
  /** update data of the table: "account" */
  update_account?: Maybe<Account_Mutation_Response>;
  /** update single row of the table: "account" */
  update_account_by_pk?: Maybe<Account>;
  /** update data of the table: "participant" */
  update_participant?: Maybe<Participant_Mutation_Response>;
  /** update single row of the table: "participant" */
  update_participant_by_pk?: Maybe<Participant>;
  /** update data of the table: "room" */
  update_room?: Maybe<Room_Mutation_Response>;
  /** update single row of the table: "room" */
  update_room_by_pk?: Maybe<Room>;
  /** update data of the table: "room_topic" */
  update_room_topic?: Maybe<Room_Topic_Mutation_Response>;
  /** update single row of the table: "room_topic" */
  update_room_topic_by_pk?: Maybe<Room_Topic>;
  /** update data of the table: "talk" */
  update_talk?: Maybe<Talk_Mutation_Response>;
  /** update single row of the table: "talk" */
  update_talk_by_pk?: Maybe<Talk>;
  /** update data of the table: "topic" */
  update_topic?: Maybe<Topic_Mutation_Response>;
  /** update single row of the table: "topic" */
  update_topic_by_pk?: Maybe<Topic>;
};


/** mutation root */
export type Mutation_RootDelete_AccountArgs = {
  where: Account_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Account_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_ParticipantArgs = {
  where: Participant_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Participant_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_RoomArgs = {
  where: Room_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Room_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_Room_TopicArgs = {
  where: Room_Topic_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Room_Topic_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_TalkArgs = {
  where: Talk_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Talk_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootDelete_TopicArgs = {
  where: Topic_Bool_Exp;
};


/** mutation root */
export type Mutation_RootDelete_Topic_By_PkArgs = {
  id: Scalars['uuid'];
};


/** mutation root */
export type Mutation_RootInsert_AccountArgs = {
  objects: Array<Account_Insert_Input>;
  on_conflict?: Maybe<Account_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Account_OneArgs = {
  object: Account_Insert_Input;
  on_conflict?: Maybe<Account_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_ParticipantArgs = {
  objects: Array<Participant_Insert_Input>;
  on_conflict?: Maybe<Participant_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Participant_OneArgs = {
  object: Participant_Insert_Input;
  on_conflict?: Maybe<Participant_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_RoomArgs = {
  objects: Array<Room_Insert_Input>;
  on_conflict?: Maybe<Room_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Room_OneArgs = {
  object: Room_Insert_Input;
  on_conflict?: Maybe<Room_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Room_TopicArgs = {
  objects: Array<Room_Topic_Insert_Input>;
  on_conflict?: Maybe<Room_Topic_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Room_Topic_OneArgs = {
  object: Room_Topic_Insert_Input;
  on_conflict?: Maybe<Room_Topic_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_TalkArgs = {
  objects: Array<Talk_Insert_Input>;
  on_conflict?: Maybe<Talk_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Talk_OneArgs = {
  object: Talk_Insert_Input;
  on_conflict?: Maybe<Talk_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_TopicArgs = {
  objects: Array<Topic_Insert_Input>;
  on_conflict?: Maybe<Topic_On_Conflict>;
};


/** mutation root */
export type Mutation_RootInsert_Topic_OneArgs = {
  object: Topic_Insert_Input;
  on_conflict?: Maybe<Topic_On_Conflict>;
};


/** mutation root */
export type Mutation_RootUpdate_AccountArgs = {
  _set?: Maybe<Account_Set_Input>;
  where: Account_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Account_By_PkArgs = {
  _set?: Maybe<Account_Set_Input>;
  pk_columns: Account_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_ParticipantArgs = {
  _set?: Maybe<Participant_Set_Input>;
  where: Participant_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Participant_By_PkArgs = {
  _set?: Maybe<Participant_Set_Input>;
  pk_columns: Participant_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_RoomArgs = {
  _inc?: Maybe<Room_Inc_Input>;
  _set?: Maybe<Room_Set_Input>;
  where: Room_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Room_By_PkArgs = {
  _inc?: Maybe<Room_Inc_Input>;
  _set?: Maybe<Room_Set_Input>;
  pk_columns: Room_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_Room_TopicArgs = {
  _set?: Maybe<Room_Topic_Set_Input>;
  where: Room_Topic_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Room_Topic_By_PkArgs = {
  _set?: Maybe<Room_Topic_Set_Input>;
  pk_columns: Room_Topic_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_TalkArgs = {
  _set?: Maybe<Talk_Set_Input>;
  where: Talk_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Talk_By_PkArgs = {
  _set?: Maybe<Talk_Set_Input>;
  pk_columns: Talk_Pk_Columns_Input;
};


/** mutation root */
export type Mutation_RootUpdate_TopicArgs = {
  _set?: Maybe<Topic_Set_Input>;
  where: Topic_Bool_Exp;
};


/** mutation root */
export type Mutation_RootUpdate_Topic_By_PkArgs = {
  _set?: Maybe<Topic_Set_Input>;
  pk_columns: Topic_Pk_Columns_Input;
};

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/**
 * 参加者: participant
 *
 *
 * columns and relationships of "participant"
 */
export type Participant = {
  __typename?: 'participant';
  /** An object relationship */
  account: Account;
  /** account_id */
  account_id: Scalars['uuid'];
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** id */
  id: Scalars['uuid'];
  /** role */
  role: Scalars['String'];
  /** An object relationship */
  room: Room;
  /** room_id */
  room_id: Scalars['uuid'];
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregated selection of "participant" */
export type Participant_Aggregate = {
  __typename?: 'participant_aggregate';
  aggregate?: Maybe<Participant_Aggregate_Fields>;
  nodes: Array<Participant>;
};

/** aggregate fields of "participant" */
export type Participant_Aggregate_Fields = {
  __typename?: 'participant_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Participant_Max_Fields>;
  min?: Maybe<Participant_Min_Fields>;
};


/** aggregate fields of "participant" */
export type Participant_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Participant_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "participant" */
export type Participant_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Participant_Max_Order_By>;
  min?: Maybe<Participant_Min_Order_By>;
};

/** input type for inserting array relation for remote table "participant" */
export type Participant_Arr_Rel_Insert_Input = {
  data: Array<Participant_Insert_Input>;
  on_conflict?: Maybe<Participant_On_Conflict>;
};

/** Boolean expression to filter rows from the table "participant". All fields are combined with a logical 'AND'. */
export type Participant_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Participant_Bool_Exp>>>;
  _not?: Maybe<Participant_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Participant_Bool_Exp>>>;
  account?: Maybe<Account_Bool_Exp>;
  account_id?: Maybe<Uuid_Comparison_Exp>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  role?: Maybe<String_Comparison_Exp>;
  room?: Maybe<Room_Bool_Exp>;
  room_id?: Maybe<Uuid_Comparison_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "participant" */
export enum Participant_Constraint {
  /** unique or primary key constraint */
  ParticipantPkc = 'participant_pkc',
  /** unique or primary key constraint */
  RoomAccountUniqueIndex = 'room_account_unique_index'
}

/** input type for inserting data into table "participant" */
export type Participant_Insert_Input = {
  account?: Maybe<Account_Obj_Rel_Insert_Input>;
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
  room?: Maybe<Room_Obj_Rel_Insert_Input>;
  room_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Participant_Max_Fields = {
  __typename?: 'participant_max_fields';
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
  room_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "participant" */
export type Participant_Max_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Participant_Min_Fields = {
  __typename?: 'participant_min_fields';
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
  room_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "participant" */
export type Participant_Min_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "participant" */
export type Participant_Mutation_Response = {
  __typename?: 'participant_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Participant>;
};

/** input type for inserting object relation for remote table "participant" */
export type Participant_Obj_Rel_Insert_Input = {
  data: Participant_Insert_Input;
  on_conflict?: Maybe<Participant_On_Conflict>;
};

/** on conflict condition type for table "participant" */
export type Participant_On_Conflict = {
  constraint: Participant_Constraint;
  update_columns: Array<Participant_Update_Column>;
  where?: Maybe<Participant_Bool_Exp>;
};

/** ordering options when selecting data from "participant" */
export type Participant_Order_By = {
  account?: Maybe<Account_Order_By>;
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  role?: Maybe<Order_By>;
  room?: Maybe<Room_Order_By>;
  room_id?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "participant" */
export type Participant_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "participant" */
export enum Participant_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "participant" */
export type Participant_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  role?: Maybe<Scalars['String']>;
  room_id?: Maybe<Scalars['uuid']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** update columns of table "participant" */
export enum Participant_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** query root */
export type Query_Root = {
  __typename?: 'query_root';
  /** fetch data from the table: "account" */
  account: Array<Account>;
  /** fetch aggregated fields from the table: "account" */
  account_aggregate: Account_Aggregate;
  /** fetch data from the table: "account" using primary key columns */
  account_by_pk?: Maybe<Account>;
  /** fetch data from the table: "participant" */
  participant: Array<Participant>;
  /** fetch aggregated fields from the table: "participant" */
  participant_aggregate: Participant_Aggregate;
  /** fetch data from the table: "participant" using primary key columns */
  participant_by_pk?: Maybe<Participant>;
  /** fetch data from the table: "room" */
  room: Array<Room>;
  /** fetch aggregated fields from the table: "room" */
  room_aggregate: Room_Aggregate;
  /** fetch data from the table: "room" using primary key columns */
  room_by_pk?: Maybe<Room>;
  /** fetch data from the table: "room_topic" */
  room_topic: Array<Room_Topic>;
  /** fetch aggregated fields from the table: "room_topic" */
  room_topic_aggregate: Room_Topic_Aggregate;
  /** fetch data from the table: "room_topic" using primary key columns */
  room_topic_by_pk?: Maybe<Room_Topic>;
  /** fetch data from the table: "talk" */
  talk: Array<Talk>;
  /** fetch aggregated fields from the table: "talk" */
  talk_aggregate: Talk_Aggregate;
  /** fetch data from the table: "talk" using primary key columns */
  talk_by_pk?: Maybe<Talk>;
  /** fetch data from the table: "topic" */
  topic: Array<Topic>;
  /** fetch aggregated fields from the table: "topic" */
  topic_aggregate: Topic_Aggregate;
  /** fetch data from the table: "topic" using primary key columns */
  topic_by_pk?: Maybe<Topic>;
};


/** query root */
export type Query_RootAccountArgs = {
  distinct_on?: Maybe<Array<Account_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Account_Order_By>>;
  where?: Maybe<Account_Bool_Exp>;
};


/** query root */
export type Query_RootAccount_AggregateArgs = {
  distinct_on?: Maybe<Array<Account_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Account_Order_By>>;
  where?: Maybe<Account_Bool_Exp>;
};


/** query root */
export type Query_RootAccount_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootParticipantArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/** query root */
export type Query_RootParticipant_AggregateArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/** query root */
export type Query_RootParticipant_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootRoomArgs = {
  distinct_on?: Maybe<Array<Room_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Order_By>>;
  where?: Maybe<Room_Bool_Exp>;
};


/** query root */
export type Query_RootRoom_AggregateArgs = {
  distinct_on?: Maybe<Array<Room_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Order_By>>;
  where?: Maybe<Room_Bool_Exp>;
};


/** query root */
export type Query_RootRoom_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootRoom_TopicArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/** query root */
export type Query_RootRoom_Topic_AggregateArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/** query root */
export type Query_RootRoom_Topic_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootTalkArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/** query root */
export type Query_RootTalk_AggregateArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/** query root */
export type Query_RootTalk_By_PkArgs = {
  id: Scalars['uuid'];
};


/** query root */
export type Query_RootTopicArgs = {
  distinct_on?: Maybe<Array<Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Topic_Order_By>>;
  where?: Maybe<Topic_Bool_Exp>;
};


/** query root */
export type Query_RootTopic_AggregateArgs = {
  distinct_on?: Maybe<Array<Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Topic_Order_By>>;
  where?: Maybe<Topic_Bool_Exp>;
};


/** query root */
export type Query_RootTopic_By_PkArgs = {
  id: Scalars['uuid'];
};

/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type Room = {
  __typename?: 'room';
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  current_talk_id?: Maybe<Scalars['uuid']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** id */
  id: Scalars['uuid'];
  /** An array relationship */
  participants: Array<Participant>;
  /** An aggregated array relationship */
  participants_aggregate: Participant_Aggregate;
  /** An array relationship */
  room_topics: Array<Room_Topic>;
  /** An aggregated array relationship */
  room_topics_aggregate: Room_Topic_Aggregate;
  status: Scalars['Int'];
  /** An object relationship */
  talk?: Maybe<Talk>;
  /** talk_time */
  talk_time?: Maybe<Scalars['Int']>;
  /** An array relationship */
  talks: Array<Talk>;
  /** An aggregated array relationship */
  talks_aggregate: Talk_Aggregate;
  /** title */
  title?: Maybe<Scalars['String']>;
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomParticipantsArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomParticipants_AggregateArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomRoom_TopicsArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomRoom_Topics_AggregateArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomTalksArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/**
 * 雑談ルーム: room
 *
 *
 * columns and relationships of "room"
 */
export type RoomTalks_AggregateArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};

/** aggregated selection of "room" */
export type Room_Aggregate = {
  __typename?: 'room_aggregate';
  aggregate?: Maybe<Room_Aggregate_Fields>;
  nodes: Array<Room>;
};

/** aggregate fields of "room" */
export type Room_Aggregate_Fields = {
  __typename?: 'room_aggregate_fields';
  avg?: Maybe<Room_Avg_Fields>;
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Room_Max_Fields>;
  min?: Maybe<Room_Min_Fields>;
  stddev?: Maybe<Room_Stddev_Fields>;
  stddev_pop?: Maybe<Room_Stddev_Pop_Fields>;
  stddev_samp?: Maybe<Room_Stddev_Samp_Fields>;
  sum?: Maybe<Room_Sum_Fields>;
  var_pop?: Maybe<Room_Var_Pop_Fields>;
  var_samp?: Maybe<Room_Var_Samp_Fields>;
  variance?: Maybe<Room_Variance_Fields>;
};


/** aggregate fields of "room" */
export type Room_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Room_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "room" */
export type Room_Aggregate_Order_By = {
  avg?: Maybe<Room_Avg_Order_By>;
  count?: Maybe<Order_By>;
  max?: Maybe<Room_Max_Order_By>;
  min?: Maybe<Room_Min_Order_By>;
  stddev?: Maybe<Room_Stddev_Order_By>;
  stddev_pop?: Maybe<Room_Stddev_Pop_Order_By>;
  stddev_samp?: Maybe<Room_Stddev_Samp_Order_By>;
  sum?: Maybe<Room_Sum_Order_By>;
  var_pop?: Maybe<Room_Var_Pop_Order_By>;
  var_samp?: Maybe<Room_Var_Samp_Order_By>;
  variance?: Maybe<Room_Variance_Order_By>;
};

/** input type for inserting array relation for remote table "room" */
export type Room_Arr_Rel_Insert_Input = {
  data: Array<Room_Insert_Input>;
  on_conflict?: Maybe<Room_On_Conflict>;
};

/** aggregate avg on columns */
export type Room_Avg_Fields = {
  __typename?: 'room_avg_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by avg() on columns of table "room" */
export type Room_Avg_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** Boolean expression to filter rows from the table "room". All fields are combined with a logical 'AND'. */
export type Room_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Room_Bool_Exp>>>;
  _not?: Maybe<Room_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Room_Bool_Exp>>>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  current_talk_id?: Maybe<Uuid_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  participants?: Maybe<Participant_Bool_Exp>;
  room_topics?: Maybe<Room_Topic_Bool_Exp>;
  status?: Maybe<Int_Comparison_Exp>;
  talk?: Maybe<Talk_Bool_Exp>;
  talk_time?: Maybe<Int_Comparison_Exp>;
  talks?: Maybe<Talk_Bool_Exp>;
  title?: Maybe<String_Comparison_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "room" */
export enum Room_Constraint {
  /** unique or primary key constraint */
  RoomPkc = 'room_pkc'
}

/** input type for incrementing integer column in table "room" */
export type Room_Inc_Input = {
  status?: Maybe<Scalars['Int']>;
  talk_time?: Maybe<Scalars['Int']>;
};

/** input type for inserting data into table "room" */
export type Room_Insert_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  current_talk_id?: Maybe<Scalars['uuid']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  participants?: Maybe<Participant_Arr_Rel_Insert_Input>;
  room_topics?: Maybe<Room_Topic_Arr_Rel_Insert_Input>;
  status?: Maybe<Scalars['Int']>;
  talk?: Maybe<Talk_Obj_Rel_Insert_Input>;
  talk_time?: Maybe<Scalars['Int']>;
  talks?: Maybe<Talk_Arr_Rel_Insert_Input>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Room_Max_Fields = {
  __typename?: 'room_max_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  current_talk_id?: Maybe<Scalars['uuid']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['Int']>;
  talk_time?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "room" */
export type Room_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  current_talk_id?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Room_Min_Fields = {
  __typename?: 'room_min_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  current_talk_id?: Maybe<Scalars['uuid']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['Int']>;
  talk_time?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "room" */
export type Room_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  current_talk_id?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
  title?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "room" */
export type Room_Mutation_Response = {
  __typename?: 'room_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Room>;
};

/** input type for inserting object relation for remote table "room" */
export type Room_Obj_Rel_Insert_Input = {
  data: Room_Insert_Input;
  on_conflict?: Maybe<Room_On_Conflict>;
};

/** on conflict condition type for table "room" */
export type Room_On_Conflict = {
  constraint: Room_Constraint;
  update_columns: Array<Room_Update_Column>;
  where?: Maybe<Room_Bool_Exp>;
};

/** ordering options when selecting data from "room" */
export type Room_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  current_talk_id?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  participants_aggregate?: Maybe<Participant_Aggregate_Order_By>;
  room_topics_aggregate?: Maybe<Room_Topic_Aggregate_Order_By>;
  status?: Maybe<Order_By>;
  talk?: Maybe<Talk_Order_By>;
  talk_time?: Maybe<Order_By>;
  talks_aggregate?: Maybe<Talk_Aggregate_Order_By>;
  title?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "room" */
export type Room_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "room" */
export enum Room_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  CurrentTalkId = 'current_talk_id',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  TalkTime = 'talk_time',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "room" */
export type Room_Set_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  current_talk_id?: Maybe<Scalars['uuid']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  status?: Maybe<Scalars['Int']>;
  talk_time?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate stddev on columns */
export type Room_Stddev_Fields = {
  __typename?: 'room_stddev_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by stddev() on columns of table "room" */
export type Room_Stddev_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** aggregate stddev_pop on columns */
export type Room_Stddev_Pop_Fields = {
  __typename?: 'room_stddev_pop_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by stddev_pop() on columns of table "room" */
export type Room_Stddev_Pop_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** aggregate stddev_samp on columns */
export type Room_Stddev_Samp_Fields = {
  __typename?: 'room_stddev_samp_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by stddev_samp() on columns of table "room" */
export type Room_Stddev_Samp_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** aggregate sum on columns */
export type Room_Sum_Fields = {
  __typename?: 'room_sum_fields';
  status?: Maybe<Scalars['Int']>;
  talk_time?: Maybe<Scalars['Int']>;
};

/** order by sum() on columns of table "room" */
export type Room_Sum_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/**
 * ルームでの話題
 *
 *
 * columns and relationships of "room_topic"
 */
export type Room_Topic = {
  __typename?: 'room_topic';
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** id */
  id: Scalars['uuid'];
  /** An object relationship */
  room: Room;
  /** room_id */
  room_id: Scalars['uuid'];
  /** An array relationship */
  talks: Array<Talk>;
  /** An aggregated array relationship */
  talks_aggregate: Talk_Aggregate;
  /** topic */
  topic: Scalars['String'];
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};


/**
 * ルームでの話題
 *
 *
 * columns and relationships of "room_topic"
 */
export type Room_TopicTalksArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/**
 * ルームでの話題
 *
 *
 * columns and relationships of "room_topic"
 */
export type Room_TopicTalks_AggregateArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};

/** aggregated selection of "room_topic" */
export type Room_Topic_Aggregate = {
  __typename?: 'room_topic_aggregate';
  aggregate?: Maybe<Room_Topic_Aggregate_Fields>;
  nodes: Array<Room_Topic>;
};

/** aggregate fields of "room_topic" */
export type Room_Topic_Aggregate_Fields = {
  __typename?: 'room_topic_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Room_Topic_Max_Fields>;
  min?: Maybe<Room_Topic_Min_Fields>;
};


/** aggregate fields of "room_topic" */
export type Room_Topic_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Room_Topic_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "room_topic" */
export type Room_Topic_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Room_Topic_Max_Order_By>;
  min?: Maybe<Room_Topic_Min_Order_By>;
};

/** input type for inserting array relation for remote table "room_topic" */
export type Room_Topic_Arr_Rel_Insert_Input = {
  data: Array<Room_Topic_Insert_Input>;
  on_conflict?: Maybe<Room_Topic_On_Conflict>;
};

/** Boolean expression to filter rows from the table "room_topic". All fields are combined with a logical 'AND'. */
export type Room_Topic_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Room_Topic_Bool_Exp>>>;
  _not?: Maybe<Room_Topic_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Room_Topic_Bool_Exp>>>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  room?: Maybe<Room_Bool_Exp>;
  room_id?: Maybe<Uuid_Comparison_Exp>;
  talks?: Maybe<Talk_Bool_Exp>;
  topic?: Maybe<String_Comparison_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "room_topic" */
export enum Room_Topic_Constraint {
  /** unique or primary key constraint */
  RoomTopicPkc = 'room_topic_pkc'
}

/** input type for inserting data into table "room_topic" */
export type Room_Topic_Insert_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  room?: Maybe<Room_Obj_Rel_Insert_Input>;
  room_id?: Maybe<Scalars['uuid']>;
  talks?: Maybe<Talk_Arr_Rel_Insert_Input>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Room_Topic_Max_Fields = {
  __typename?: 'room_topic_max_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "room_topic" */
export type Room_Topic_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Room_Topic_Min_Fields = {
  __typename?: 'room_topic_min_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "room_topic" */
export type Room_Topic_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "room_topic" */
export type Room_Topic_Mutation_Response = {
  __typename?: 'room_topic_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Room_Topic>;
};

/** input type for inserting object relation for remote table "room_topic" */
export type Room_Topic_Obj_Rel_Insert_Input = {
  data: Room_Topic_Insert_Input;
  on_conflict?: Maybe<Room_Topic_On_Conflict>;
};

/** on conflict condition type for table "room_topic" */
export type Room_Topic_On_Conflict = {
  constraint: Room_Topic_Constraint;
  update_columns: Array<Room_Topic_Update_Column>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};

/** ordering options when selecting data from "room_topic" */
export type Room_Topic_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room?: Maybe<Room_Order_By>;
  room_id?: Maybe<Order_By>;
  talks_aggregate?: Maybe<Talk_Aggregate_Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "room_topic" */
export type Room_Topic_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "room_topic" */
export enum Room_Topic_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  Topic = 'topic',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "room_topic" */
export type Room_Topic_Set_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** update columns of table "room_topic" */
export enum Room_Topic_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  Topic = 'topic',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** update columns of table "room" */
export enum Room_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  CurrentTalkId = 'current_talk_id',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  TalkTime = 'talk_time',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** aggregate var_pop on columns */
export type Room_Var_Pop_Fields = {
  __typename?: 'room_var_pop_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by var_pop() on columns of table "room" */
export type Room_Var_Pop_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** aggregate var_samp on columns */
export type Room_Var_Samp_Fields = {
  __typename?: 'room_var_samp_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by var_samp() on columns of table "room" */
export type Room_Var_Samp_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** aggregate variance on columns */
export type Room_Variance_Fields = {
  __typename?: 'room_variance_fields';
  status?: Maybe<Scalars['Float']>;
  talk_time?: Maybe<Scalars['Float']>;
};

/** order by variance() on columns of table "room" */
export type Room_Variance_Order_By = {
  status?: Maybe<Order_By>;
  talk_time?: Maybe<Order_By>;
};

/** subscription root */
export type Subscription_Root = {
  __typename?: 'subscription_root';
  /** fetch data from the table: "account" */
  account: Array<Account>;
  /** fetch aggregated fields from the table: "account" */
  account_aggregate: Account_Aggregate;
  /** fetch data from the table: "account" using primary key columns */
  account_by_pk?: Maybe<Account>;
  /** fetch data from the table: "participant" */
  participant: Array<Participant>;
  /** fetch aggregated fields from the table: "participant" */
  participant_aggregate: Participant_Aggregate;
  /** fetch data from the table: "participant" using primary key columns */
  participant_by_pk?: Maybe<Participant>;
  /** fetch data from the table: "room" */
  room: Array<Room>;
  /** fetch aggregated fields from the table: "room" */
  room_aggregate: Room_Aggregate;
  /** fetch data from the table: "room" using primary key columns */
  room_by_pk?: Maybe<Room>;
  /** fetch data from the table: "room_topic" */
  room_topic: Array<Room_Topic>;
  /** fetch aggregated fields from the table: "room_topic" */
  room_topic_aggregate: Room_Topic_Aggregate;
  /** fetch data from the table: "room_topic" using primary key columns */
  room_topic_by_pk?: Maybe<Room_Topic>;
  /** fetch data from the table: "talk" */
  talk: Array<Talk>;
  /** fetch aggregated fields from the table: "talk" */
  talk_aggregate: Talk_Aggregate;
  /** fetch data from the table: "talk" using primary key columns */
  talk_by_pk?: Maybe<Talk>;
  /** fetch data from the table: "topic" */
  topic: Array<Topic>;
  /** fetch aggregated fields from the table: "topic" */
  topic_aggregate: Topic_Aggregate;
  /** fetch data from the table: "topic" using primary key columns */
  topic_by_pk?: Maybe<Topic>;
};


/** subscription root */
export type Subscription_RootAccountArgs = {
  distinct_on?: Maybe<Array<Account_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Account_Order_By>>;
  where?: Maybe<Account_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAccount_AggregateArgs = {
  distinct_on?: Maybe<Array<Account_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Account_Order_By>>;
  where?: Maybe<Account_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootAccount_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootParticipantArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootParticipant_AggregateArgs = {
  distinct_on?: Maybe<Array<Participant_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Participant_Order_By>>;
  where?: Maybe<Participant_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootParticipant_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootRoomArgs = {
  distinct_on?: Maybe<Array<Room_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Order_By>>;
  where?: Maybe<Room_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRoom_AggregateArgs = {
  distinct_on?: Maybe<Array<Room_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Order_By>>;
  where?: Maybe<Room_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRoom_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootRoom_TopicArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRoom_Topic_AggregateArgs = {
  distinct_on?: Maybe<Array<Room_Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Room_Topic_Order_By>>;
  where?: Maybe<Room_Topic_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootRoom_Topic_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootTalkArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootTalk_AggregateArgs = {
  distinct_on?: Maybe<Array<Talk_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Talk_Order_By>>;
  where?: Maybe<Talk_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootTalk_By_PkArgs = {
  id: Scalars['uuid'];
};


/** subscription root */
export type Subscription_RootTopicArgs = {
  distinct_on?: Maybe<Array<Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Topic_Order_By>>;
  where?: Maybe<Topic_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootTopic_AggregateArgs = {
  distinct_on?: Maybe<Array<Topic_Select_Column>>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order_by?: Maybe<Array<Topic_Order_By>>;
  where?: Maybe<Topic_Bool_Exp>;
};


/** subscription root */
export type Subscription_RootTopic_By_PkArgs = {
  id: Scalars['uuid'];
};

/**
 * 雑談: talk
 *
 *
 * columns and relationships of "talk"
 */
export type Talk = {
  __typename?: 'talk';
  /** An object relationship */
  account: Account;
  /** account_id */
  account_id: Scalars['uuid'];
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** finished_at */
  finished_at?: Maybe<Scalars['timestamp']>;
  /** id */
  id: Scalars['uuid'];
  /** An object relationship */
  room: Room;
  /** room_id */
  room_id: Scalars['uuid'];
  /** An object relationship */
  room_topic?: Maybe<Room_Topic>;
  /** room_topic_id */
  room_topic_id?: Maybe<Scalars['uuid']>;
  /** started_at */
  started_at?: Maybe<Scalars['timestamp']>;
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregated selection of "talk" */
export type Talk_Aggregate = {
  __typename?: 'talk_aggregate';
  aggregate?: Maybe<Talk_Aggregate_Fields>;
  nodes: Array<Talk>;
};

/** aggregate fields of "talk" */
export type Talk_Aggregate_Fields = {
  __typename?: 'talk_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Talk_Max_Fields>;
  min?: Maybe<Talk_Min_Fields>;
};


/** aggregate fields of "talk" */
export type Talk_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Talk_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "talk" */
export type Talk_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Talk_Max_Order_By>;
  min?: Maybe<Talk_Min_Order_By>;
};

/** input type for inserting array relation for remote table "talk" */
export type Talk_Arr_Rel_Insert_Input = {
  data: Array<Talk_Insert_Input>;
  on_conflict?: Maybe<Talk_On_Conflict>;
};

/** Boolean expression to filter rows from the table "talk". All fields are combined with a logical 'AND'. */
export type Talk_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Talk_Bool_Exp>>>;
  _not?: Maybe<Talk_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Talk_Bool_Exp>>>;
  account?: Maybe<Account_Bool_Exp>;
  account_id?: Maybe<Uuid_Comparison_Exp>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  finished_at?: Maybe<Timestamp_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  room?: Maybe<Room_Bool_Exp>;
  room_id?: Maybe<Uuid_Comparison_Exp>;
  room_topic?: Maybe<Room_Topic_Bool_Exp>;
  room_topic_id?: Maybe<Uuid_Comparison_Exp>;
  started_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "talk" */
export enum Talk_Constraint {
  /** unique or primary key constraint */
  TalkPkc = 'talk_pkc'
}

/** input type for inserting data into table "talk" */
export type Talk_Insert_Input = {
  account?: Maybe<Account_Obj_Rel_Insert_Input>;
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  finished_at?: Maybe<Scalars['timestamp']>;
  id?: Maybe<Scalars['uuid']>;
  room?: Maybe<Room_Obj_Rel_Insert_Input>;
  room_id?: Maybe<Scalars['uuid']>;
  room_topic?: Maybe<Room_Topic_Obj_Rel_Insert_Input>;
  room_topic_id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamp']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Talk_Max_Fields = {
  __typename?: 'talk_max_fields';
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  finished_at?: Maybe<Scalars['timestamp']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  room_topic_id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamp']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "talk" */
export type Talk_Max_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  finished_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  room_topic_id?: Maybe<Order_By>;
  started_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Talk_Min_Fields = {
  __typename?: 'talk_min_fields';
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  finished_at?: Maybe<Scalars['timestamp']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  room_topic_id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamp']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "talk" */
export type Talk_Min_Order_By = {
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  finished_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room_id?: Maybe<Order_By>;
  room_topic_id?: Maybe<Order_By>;
  started_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "talk" */
export type Talk_Mutation_Response = {
  __typename?: 'talk_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Talk>;
};

/** input type for inserting object relation for remote table "talk" */
export type Talk_Obj_Rel_Insert_Input = {
  data: Talk_Insert_Input;
  on_conflict?: Maybe<Talk_On_Conflict>;
};

/** on conflict condition type for table "talk" */
export type Talk_On_Conflict = {
  constraint: Talk_Constraint;
  update_columns: Array<Talk_Update_Column>;
  where?: Maybe<Talk_Bool_Exp>;
};

/** ordering options when selecting data from "talk" */
export type Talk_Order_By = {
  account?: Maybe<Account_Order_By>;
  account_id?: Maybe<Order_By>;
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  finished_at?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  room?: Maybe<Room_Order_By>;
  room_id?: Maybe<Order_By>;
  room_topic?: Maybe<Room_Topic_Order_By>;
  room_topic_id?: Maybe<Order_By>;
  started_at?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "talk" */
export type Talk_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "talk" */
export enum Talk_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  FinishedAt = 'finished_at',
  /** column name */
  Id = 'id',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  RoomTopicId = 'room_topic_id',
  /** column name */
  StartedAt = 'started_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "talk" */
export type Talk_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>;
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  finished_at?: Maybe<Scalars['timestamp']>;
  id?: Maybe<Scalars['uuid']>;
  room_id?: Maybe<Scalars['uuid']>;
  room_topic_id?: Maybe<Scalars['uuid']>;
  started_at?: Maybe<Scalars['timestamp']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** update columns of table "talk" */
export enum Talk_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  FinishedAt = 'finished_at',
  /** column name */
  Id = 'id',
  /** column name */
  RoomId = 'room_id',
  /** column name */
  RoomTopicId = 'room_topic_id',
  /** column name */
  StartedAt = 'started_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}


/** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
export type Timestamp_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamp']>;
  _gt?: Maybe<Scalars['timestamp']>;
  _gte?: Maybe<Scalars['timestamp']>;
  _in?: Maybe<Array<Scalars['timestamp']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['timestamp']>;
  _lte?: Maybe<Scalars['timestamp']>;
  _neq?: Maybe<Scalars['timestamp']>;
  _nin?: Maybe<Array<Scalars['timestamp']>>;
};

/**
 * デフォルトの話題: topic
 *
 *
 * columns and relationships of "topic"
 */
export type Topic = {
  __typename?: 'topic';
  /** created_at */
  created_at: Scalars['timestamp'];
  /** created_by */
  created_by?: Maybe<Scalars['String']>;
  /** deleted_at */
  deleted_at: Scalars['timestamp'];
  /** deleted_by */
  deleted_by?: Maybe<Scalars['String']>;
  /** id */
  id: Scalars['uuid'];
  /** topic */
  topic: Scalars['String'];
  /** updated_at */
  updated_at: Scalars['timestamp'];
  /** updated_by */
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregated selection of "topic" */
export type Topic_Aggregate = {
  __typename?: 'topic_aggregate';
  aggregate?: Maybe<Topic_Aggregate_Fields>;
  nodes: Array<Topic>;
};

/** aggregate fields of "topic" */
export type Topic_Aggregate_Fields = {
  __typename?: 'topic_aggregate_fields';
  count?: Maybe<Scalars['Int']>;
  max?: Maybe<Topic_Max_Fields>;
  min?: Maybe<Topic_Min_Fields>;
};


/** aggregate fields of "topic" */
export type Topic_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Topic_Select_Column>>;
  distinct?: Maybe<Scalars['Boolean']>;
};

/** order by aggregate values of table "topic" */
export type Topic_Aggregate_Order_By = {
  count?: Maybe<Order_By>;
  max?: Maybe<Topic_Max_Order_By>;
  min?: Maybe<Topic_Min_Order_By>;
};

/** input type for inserting array relation for remote table "topic" */
export type Topic_Arr_Rel_Insert_Input = {
  data: Array<Topic_Insert_Input>;
  on_conflict?: Maybe<Topic_On_Conflict>;
};

/** Boolean expression to filter rows from the table "topic". All fields are combined with a logical 'AND'. */
export type Topic_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Topic_Bool_Exp>>>;
  _not?: Maybe<Topic_Bool_Exp>;
  _or?: Maybe<Array<Maybe<Topic_Bool_Exp>>>;
  created_at?: Maybe<Timestamp_Comparison_Exp>;
  created_by?: Maybe<String_Comparison_Exp>;
  deleted_at?: Maybe<Timestamp_Comparison_Exp>;
  deleted_by?: Maybe<String_Comparison_Exp>;
  id?: Maybe<Uuid_Comparison_Exp>;
  topic?: Maybe<String_Comparison_Exp>;
  updated_at?: Maybe<Timestamp_Comparison_Exp>;
  updated_by?: Maybe<String_Comparison_Exp>;
};

/** unique or primary key constraints on table "topic" */
export enum Topic_Constraint {
  /** unique or primary key constraint */
  TopicPkc = 'topic_pkc'
}

/** input type for inserting data into table "topic" */
export type Topic_Insert_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** aggregate max on columns */
export type Topic_Max_Fields = {
  __typename?: 'topic_max_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by max() on columns of table "topic" */
export type Topic_Max_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** aggregate min on columns */
export type Topic_Min_Fields = {
  __typename?: 'topic_min_fields';
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** order by min() on columns of table "topic" */
export type Topic_Min_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** response of any mutation on the table "topic" */
export type Topic_Mutation_Response = {
  __typename?: 'topic_mutation_response';
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int'];
  /** data of the affected rows by the mutation */
  returning: Array<Topic>;
};

/** input type for inserting object relation for remote table "topic" */
export type Topic_Obj_Rel_Insert_Input = {
  data: Topic_Insert_Input;
  on_conflict?: Maybe<Topic_On_Conflict>;
};

/** on conflict condition type for table "topic" */
export type Topic_On_Conflict = {
  constraint: Topic_Constraint;
  update_columns: Array<Topic_Update_Column>;
  where?: Maybe<Topic_Bool_Exp>;
};

/** ordering options when selecting data from "topic" */
export type Topic_Order_By = {
  created_at?: Maybe<Order_By>;
  created_by?: Maybe<Order_By>;
  deleted_at?: Maybe<Order_By>;
  deleted_by?: Maybe<Order_By>;
  id?: Maybe<Order_By>;
  topic?: Maybe<Order_By>;
  updated_at?: Maybe<Order_By>;
  updated_by?: Maybe<Order_By>;
};

/** primary key columns input for table: "topic" */
export type Topic_Pk_Columns_Input = {
  /** id */
  id: Scalars['uuid'];
};

/** select columns of table "topic" */
export enum Topic_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Topic = 'topic',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}

/** input type for updating data in table "topic" */
export type Topic_Set_Input = {
  created_at?: Maybe<Scalars['timestamp']>;
  created_by?: Maybe<Scalars['String']>;
  deleted_at?: Maybe<Scalars['timestamp']>;
  deleted_by?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['uuid']>;
  topic?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['timestamp']>;
  updated_by?: Maybe<Scalars['String']>;
};

/** update columns of table "topic" */
export enum Topic_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CreatedBy = 'created_by',
  /** column name */
  DeletedAt = 'deleted_at',
  /** column name */
  DeletedBy = 'deleted_by',
  /** column name */
  Id = 'id',
  /** column name */
  Topic = 'topic',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UpdatedBy = 'updated_by'
}


/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: Maybe<Scalars['uuid']>;
  _gt?: Maybe<Scalars['uuid']>;
  _gte?: Maybe<Scalars['uuid']>;
  _in?: Maybe<Array<Scalars['uuid']>>;
  _is_null?: Maybe<Scalars['Boolean']>;
  _lt?: Maybe<Scalars['uuid']>;
  _lte?: Maybe<Scalars['uuid']>;
  _neq?: Maybe<Scalars['uuid']>;
  _nin?: Maybe<Array<Scalars['uuid']>>;
};

export type Account_By_PkQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type Account_By_PkQuery = (
  { __typename?: 'query_root' }
  & { account_by_pk?: Maybe<(
    { __typename?: 'account' }
    & Pick<Account, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'nickname' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Account_OneMutationVariables = Exact<{
  object: Account_Insert_Input;
}>;


export type Insert_Account_OneMutation = (
  { __typename?: 'mutation_root' }
  & { insert_account_one?: Maybe<(
    { __typename?: 'account' }
    & Pick<Account, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'nickname' | 'updated_at' | 'updated_by'>
  )> }
);

export type Update_Account_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
  _set: Account_Set_Input;
}>;


export type Update_Account_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { update_account_by_pk?: Maybe<(
    { __typename?: 'account' }
    & Pick<Account, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'nickname' | 'updated_at' | 'updated_by'>
  )> }
);

export type Delete_Participant_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type Delete_Participant_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { delete_participant_by_pk?: Maybe<(
    { __typename?: 'participant' }
    & Pick<Participant, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'role' | 'room_id' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Participant_OneMutationVariables = Exact<{
  object: Participant_Insert_Input;
}>;


export type Insert_Participant_OneMutation = (
  { __typename?: 'mutation_root' }
  & { insert_participant_one?: Maybe<(
    { __typename?: 'participant' }
    & Pick<Participant, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'role' | 'room_id' | 'updated_at' | 'updated_by'>
  )> }
);

export type Update_Participant_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
  _set: Participant_Set_Input;
}>;


export type Update_Participant_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { update_participant_by_pk?: Maybe<(
    { __typename?: 'participant' }
    & Pick<Participant, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'role' | 'room_id' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Room_OneMutationVariables = Exact<{
  object: Room_Insert_Input;
}>;


export type Insert_Room_OneMutation = (
  { __typename?: 'mutation_root' }
  & { insert_room_one?: Maybe<(
    { __typename?: 'room' }
    & Pick<Room, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'talk_time' | 'title' | 'updated_at' | 'updated_by' | 'current_talk_id' | 'status'>
  )> }
);

export type Room_By_PkQueryVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type Room_By_PkQuery = (
  { __typename?: 'query_root' }
  & { room_by_pk?: Maybe<(
    { __typename?: 'room' }
    & Pick<Room, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'talk_time' | 'title' | 'updated_at' | 'updated_by' | 'current_talk_id' | 'status'>
    & { participants: Array<(
      { __typename?: 'participant' }
      & Pick<Participant, 'account_id' | 'role' | 'room_id' | 'id'>
      & { account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )>, room_topics: Array<(
      { __typename?: 'room_topic' }
      & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
    )>, talks: Array<(
      { __typename?: 'talk' }
      & Pick<Talk, 'account_id' | 'id' | 'finished_at' | 'room_id' | 'room_topic_id' | 'started_at'>
      & { room_topic?: Maybe<(
        { __typename?: 'room_topic' }
        & Pick<Room_Topic, 'id' | 'topic'>
      )>, account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )>, talk?: Maybe<(
      { __typename?: 'talk' }
      & Pick<Talk, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'finished_at' | 'id' | 'room_id' | 'room_topic_id' | 'started_at' | 'updated_at' | 'updated_by'>
      & { room_topic?: Maybe<(
        { __typename?: 'room_topic' }
        & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
      )>, account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )> }
  )> }
);

export type Subscribe_Room_By_PkSubscriptionVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type Subscribe_Room_By_PkSubscription = (
  { __typename?: 'subscription_root' }
  & { room_by_pk?: Maybe<(
    { __typename?: 'room' }
    & Pick<Room, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'talk_time' | 'title' | 'updated_at' | 'updated_by' | 'current_talk_id' | 'status'>
    & { participants: Array<(
      { __typename?: 'participant' }
      & Pick<Participant, 'account_id' | 'role' | 'room_id' | 'id'>
      & { account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )>, room_topics: Array<(
      { __typename?: 'room_topic' }
      & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
    )>, talks: Array<(
      { __typename?: 'talk' }
      & Pick<Talk, 'account_id' | 'id' | 'finished_at' | 'room_id' | 'room_topic_id' | 'started_at'>
      & { room_topic?: Maybe<(
        { __typename?: 'room_topic' }
        & Pick<Room_Topic, 'id' | 'topic'>
      )>, account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )>, talk?: Maybe<(
      { __typename?: 'talk' }
      & Pick<Talk, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'finished_at' | 'id' | 'room_id' | 'room_topic_id' | 'started_at' | 'updated_at' | 'updated_by'>
      & { room_topic?: Maybe<(
        { __typename?: 'room_topic' }
        & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
      )>, account: (
        { __typename?: 'account' }
        & Pick<Account, 'nickname' | 'id'>
      ) }
    )> }
  )> }
);

export type Update_Room_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
  _set: Room_Set_Input;
}>;


export type Update_Room_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { update_room_by_pk?: Maybe<(
    { __typename?: 'room' }
    & Pick<Room, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'current_talk_id' | 'status'>
  )> }
);

export type Delete_Room_Topic_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
}>;


export type Delete_Room_Topic_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { delete_room_topic_by_pk?: Maybe<(
    { __typename?: 'room_topic' }
    & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Room_Topic_OneMutationVariables = Exact<{
  object: Room_Topic_Insert_Input;
}>;


export type Insert_Room_Topic_OneMutation = (
  { __typename?: 'mutation_root' }
  & { insert_room_topic_one?: Maybe<(
    { __typename?: 'room_topic' }
    & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Room_TopicsMutationVariables = Exact<{
  objects: Array<Room_Topic_Insert_Input> | Room_Topic_Insert_Input;
}>;


export type Insert_Room_TopicsMutation = (
  { __typename?: 'mutation_root' }
  & { insert_room_topic?: Maybe<(
    { __typename?: 'room_topic_mutation_response' }
    & { returning: Array<(
      { __typename?: 'room_topic' }
      & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
    )> }
  )> }
);

export type Update_Room_Topic_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
  _set: Room_Topic_Set_Input;
}>;


export type Update_Room_Topic_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { update_room_topic_by_pk?: Maybe<(
    { __typename?: 'room_topic' }
    & Pick<Room_Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'room_id' | 'topic' | 'updated_at' | 'updated_by'>
  )> }
);

export type Insert_Talk_OneMutationVariables = Exact<{
  object: Talk_Insert_Input;
}>;


export type Insert_Talk_OneMutation = (
  { __typename?: 'mutation_root' }
  & { insert_talk_one?: Maybe<(
    { __typename?: 'talk' }
    & Pick<Talk, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'finished_at' | 'deleted_by' | 'id' | 'room_id' | 'room_topic_id' | 'started_at' | 'updated_at' | 'updated_by'>
  )> }
);

export type Update_Talk_By_PkMutationVariables = Exact<{
  id: Scalars['uuid'];
  _set: Talk_Set_Input;
}>;


export type Update_Talk_By_PkMutation = (
  { __typename?: 'mutation_root' }
  & { update_talk_by_pk?: Maybe<(
    { __typename?: 'talk' }
    & Pick<Talk, 'account_id' | 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'finished_at' | 'id' | 'room_id' | 'room_topic_id' | 'started_at' | 'updated_at' | 'updated_by'>
  )> }
);

export type Topic_AllQueryVariables = Exact<{ [key: string]: never; }>;


export type Topic_AllQuery = (
  { __typename?: 'query_root' }
  & { topic: Array<(
    { __typename?: 'topic' }
    & Pick<Topic, 'created_at' | 'created_by' | 'deleted_at' | 'deleted_by' | 'id' | 'topic' | 'updated_at' | 'updated_by'>
  )> }
);


export const Account_By_PkDocument = gql`
    query account_by_pk($id: uuid!) {
  account_by_pk(id: $id) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    nickname
    updated_at
    updated_by
  }
}
    `;
export const Insert_Account_OneDocument = gql`
    mutation insert_account_one($object: account_insert_input!) {
  insert_account_one(object: $object) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    nickname
    updated_at
    updated_by
  }
}
    `;
export const Update_Account_By_PkDocument = gql`
    mutation update_account_by_pk($id: uuid!, $_set: account_set_input!) {
  update_account_by_pk(_set: $_set, pk_columns: {id: $id}) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    nickname
    updated_at
    updated_by
  }
}
    `;
export const Delete_Participant_By_PkDocument = gql`
    mutation delete_participant_by_pk($id: uuid!) {
  delete_participant_by_pk(id: $id) {
    account_id
    created_at
    created_by
    deleted_at
    deleted_by
    id
    role
    room_id
    updated_at
    updated_by
  }
}
    `;
export const Insert_Participant_OneDocument = gql`
    mutation insert_participant_one($object: participant_insert_input!) {
  insert_participant_one(object: $object) {
    account_id
    created_at
    created_by
    deleted_at
    deleted_by
    id
    role
    room_id
    updated_at
    updated_by
  }
}
    `;
export const Update_Participant_By_PkDocument = gql`
    mutation update_participant_by_pk($id: uuid!, $_set: participant_set_input!) {
  update_participant_by_pk(pk_columns: {id: $id}, _set: $_set) {
    account_id
    created_at
    created_by
    deleted_at
    deleted_by
    id
    role
    room_id
    updated_at
    updated_by
  }
}
    `;
export const Insert_Room_OneDocument = gql`
    mutation insert_room_one($object: room_insert_input!) {
  insert_room_one(object: $object) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    talk_time
    title
    updated_at
    updated_by
    current_talk_id
    status
  }
}
    `;
export const Room_By_PkDocument = gql`
    query room_by_pk($id: uuid!) {
  room_by_pk(id: $id) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    talk_time
    title
    updated_at
    updated_by
    participants {
      account_id
      account {
        nickname
        id
      }
      role
      room_id
      id
    }
    room_topics {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      room_id
      topic
      updated_at
      updated_by
    }
    talks {
      account_id
      id
      finished_at
      room_id
      room_topic_id
      started_at
      room_topic {
        id
        topic
      }
      account {
        nickname
        id
      }
    }
    current_talk_id
    talk {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      finished_at
      id
      room_id
      room_topic_id
      started_at
      updated_at
      updated_by
      room_topic {
        created_at
        created_by
        deleted_at
        deleted_by
        id
        room_id
        topic
        updated_at
        updated_by
      }
      account {
        nickname
        id
      }
    }
    status
  }
}
    `;
export const Subscribe_Room_By_PkDocument = gql`
    subscription subscribe_room_by_pk($id: uuid!) {
  room_by_pk(id: $id) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    talk_time
    title
    updated_at
    updated_by
    participants {
      account_id
      account {
        nickname
        id
      }
      role
      room_id
      id
    }
    room_topics {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      room_id
      topic
      updated_at
      updated_by
    }
    talks {
      account_id
      id
      finished_at
      room_id
      room_topic_id
      started_at
      room_topic {
        id
        topic
      }
      account {
        nickname
        id
      }
    }
    current_talk_id
    talk {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      finished_at
      id
      room_id
      room_topic_id
      started_at
      updated_at
      updated_by
      room_topic {
        created_at
        created_by
        deleted_at
        deleted_by
        id
        room_id
        topic
        updated_at
        updated_by
      }
      account {
        nickname
        id
      }
    }
    status
  }
}
    `;
export const Update_Room_By_PkDocument = gql`
    mutation update_room_by_pk($id: uuid!, $_set: room_set_input!) {
  update_room_by_pk(pk_columns: {id: $id}, _set: $_set) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    current_talk_id
    status
  }
}
    `;
export const Delete_Room_Topic_By_PkDocument = gql`
    mutation delete_room_topic_by_pk($id: uuid!) {
  delete_room_topic_by_pk(id: $id) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    room_id
    topic
    updated_at
    updated_by
  }
}
    `;
export const Insert_Room_Topic_OneDocument = gql`
    mutation insert_room_topic_one($object: room_topic_insert_input!) {
  insert_room_topic_one(object: $object) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    room_id
    topic
    updated_at
    updated_by
  }
}
    `;
export const Insert_Room_TopicsDocument = gql`
    mutation insert_room_topics($objects: [room_topic_insert_input!]!) {
  insert_room_topic(objects: $objects) {
    returning {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      room_id
      topic
      updated_at
      updated_by
    }
  }
}
    `;
export const Update_Room_Topic_By_PkDocument = gql`
    mutation update_room_topic_by_pk($id: uuid!, $_set: room_topic_set_input!) {
  update_room_topic_by_pk(pk_columns: {id: $id}, _set: $_set) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    room_id
    topic
    updated_at
    updated_by
  }
}
    `;
export const Insert_Talk_OneDocument = gql`
    mutation insert_talk_one($object: talk_insert_input!) {
  insert_talk_one(object: $object) {
    account_id
    created_at
    created_by
    deleted_at
    finished_at
    deleted_by
    id
    room_id
    room_topic_id
    started_at
    updated_at
    updated_by
  }
}
    `;
export const Update_Talk_By_PkDocument = gql`
    mutation update_talk_by_pk($id: uuid!, $_set: talk_set_input!) {
  update_talk_by_pk(pk_columns: {id: $id}, _set: $_set) {
    account_id
    created_at
    created_by
    deleted_at
    deleted_by
    finished_at
    id
    room_id
    room_topic_id
    started_at
    updated_at
    updated_by
  }
}
    `;
export const Topic_AllDocument = gql`
    query topic_all {
  topic {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    topic
    updated_at
    updated_by
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();
export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    account_by_pk(variables: Account_By_PkQueryVariables): Promise<Account_By_PkQuery> {
      return withWrapper(() => client.request<Account_By_PkQuery>(print(Account_By_PkDocument), variables));
    },
    insert_account_one(variables: Insert_Account_OneMutationVariables): Promise<Insert_Account_OneMutation> {
      return withWrapper(() => client.request<Insert_Account_OneMutation>(print(Insert_Account_OneDocument), variables));
    },
    update_account_by_pk(variables: Update_Account_By_PkMutationVariables): Promise<Update_Account_By_PkMutation> {
      return withWrapper(() => client.request<Update_Account_By_PkMutation>(print(Update_Account_By_PkDocument), variables));
    },
    delete_participant_by_pk(variables: Delete_Participant_By_PkMutationVariables): Promise<Delete_Participant_By_PkMutation> {
      return withWrapper(() => client.request<Delete_Participant_By_PkMutation>(print(Delete_Participant_By_PkDocument), variables));
    },
    insert_participant_one(variables: Insert_Participant_OneMutationVariables): Promise<Insert_Participant_OneMutation> {
      return withWrapper(() => client.request<Insert_Participant_OneMutation>(print(Insert_Participant_OneDocument), variables));
    },
    update_participant_by_pk(variables: Update_Participant_By_PkMutationVariables): Promise<Update_Participant_By_PkMutation> {
      return withWrapper(() => client.request<Update_Participant_By_PkMutation>(print(Update_Participant_By_PkDocument), variables));
    },
    insert_room_one(variables: Insert_Room_OneMutationVariables): Promise<Insert_Room_OneMutation> {
      return withWrapper(() => client.request<Insert_Room_OneMutation>(print(Insert_Room_OneDocument), variables));
    },
    room_by_pk(variables: Room_By_PkQueryVariables): Promise<Room_By_PkQuery> {
      return withWrapper(() => client.request<Room_By_PkQuery>(print(Room_By_PkDocument), variables));
    },
    subscribe_room_by_pk(variables: Subscribe_Room_By_PkSubscriptionVariables): Promise<Subscribe_Room_By_PkSubscription> {
      return withWrapper(() => client.request<Subscribe_Room_By_PkSubscription>(print(Subscribe_Room_By_PkDocument), variables));
    },
    update_room_by_pk(variables: Update_Room_By_PkMutationVariables): Promise<Update_Room_By_PkMutation> {
      return withWrapper(() => client.request<Update_Room_By_PkMutation>(print(Update_Room_By_PkDocument), variables));
    },
    delete_room_topic_by_pk(variables: Delete_Room_Topic_By_PkMutationVariables): Promise<Delete_Room_Topic_By_PkMutation> {
      return withWrapper(() => client.request<Delete_Room_Topic_By_PkMutation>(print(Delete_Room_Topic_By_PkDocument), variables));
    },
    insert_room_topic_one(variables: Insert_Room_Topic_OneMutationVariables): Promise<Insert_Room_Topic_OneMutation> {
      return withWrapper(() => client.request<Insert_Room_Topic_OneMutation>(print(Insert_Room_Topic_OneDocument), variables));
    },
    insert_room_topics(variables: Insert_Room_TopicsMutationVariables): Promise<Insert_Room_TopicsMutation> {
      return withWrapper(() => client.request<Insert_Room_TopicsMutation>(print(Insert_Room_TopicsDocument), variables));
    },
    update_room_topic_by_pk(variables: Update_Room_Topic_By_PkMutationVariables): Promise<Update_Room_Topic_By_PkMutation> {
      return withWrapper(() => client.request<Update_Room_Topic_By_PkMutation>(print(Update_Room_Topic_By_PkDocument), variables));
    },
    insert_talk_one(variables: Insert_Talk_OneMutationVariables): Promise<Insert_Talk_OneMutation> {
      return withWrapper(() => client.request<Insert_Talk_OneMutation>(print(Insert_Talk_OneDocument), variables));
    },
    update_talk_by_pk(variables: Update_Talk_By_PkMutationVariables): Promise<Update_Talk_By_PkMutation> {
      return withWrapper(() => client.request<Update_Talk_By_PkMutation>(print(Update_Talk_By_PkDocument), variables));
    },
    topic_all(variables?: Topic_AllQueryVariables): Promise<Topic_AllQuery> {
      return withWrapper(() => client.request<Topic_AllQuery>(print(Topic_AllDocument), variables));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;