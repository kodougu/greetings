import gql from 'graphql-tag'

export const update_room_by_pk = gql`
  mutation update_room_by_pk($id: uuid!, $_set: room_set_input!) {
  update_room_by_pk(pk_columns: {id: $id}, _set: $_set) {
    created_at
    created_by
    deleted_at
    deleted_by
    id
    current_talk_id
    status
  }
}

`
