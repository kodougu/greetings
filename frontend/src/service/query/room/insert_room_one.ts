import gql from 'graphql-tag'

export const insert_room_one = gql`
  mutation insert_room_one($object: room_insert_input!) {
    insert_room_one(object: $object) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      talk_time
      title
      updated_at
      updated_by
      current_talk_id
      status
    }
  }
`
