import gql from 'graphql-tag'

export const subscribe_room_by_pk = gql`
  subscription subscribe_room_by_pk($id: uuid!) {
    room_by_pk(id: $id) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      talk_time
      title
      updated_at
      updated_by
      participants {
        account_id
        account {
          nickname
          id
        }
        role
        room_id
        id
      }
      room_topics {
        created_at
        created_by
        deleted_at
        deleted_by
        id
        room_id
        topic
        updated_at
        updated_by
      }
      talks {
        account_id
        id
        finished_at
        room_id
        room_topic_id
        started_at
        room_topic {
          id
          topic
        }
        account {
          nickname
          id
        }
      }
      current_talk_id
      talk {
        account_id
        created_at
        created_by
        deleted_at
        deleted_by
        finished_at
        id
        room_id
        room_topic_id
        started_at
        updated_at
        updated_by
        room_topic {
          created_at
          created_by
          deleted_at
          deleted_by
          id
          room_id
          topic
          updated_at
          updated_by
        }
        account {
          nickname
          id
        }
      }
      status
    }
  }
`
