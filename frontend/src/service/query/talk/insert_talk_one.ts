import gql from 'graphql-tag'

export const insert_talk_one = gql`
  mutation insert_talk_one($object: talk_insert_input!) {
    insert_talk_one(object: $object) {
      account_id
      created_at
      created_by
      deleted_at
      finished_at
      deleted_by
      id
      room_id
      room_topic_id
      started_at
      updated_at
      updated_by
    }
  }
`
