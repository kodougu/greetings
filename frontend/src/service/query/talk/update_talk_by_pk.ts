import gql from 'graphql-tag'

export const update_talk_by_pk = gql`
  mutation update_talk_by_pk($id: uuid!, $_set: talk_set_input!) {
    update_talk_by_pk(pk_columns: { id: $id }, _set: $_set) {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      finished_at
      id
      room_id
      room_topic_id
      started_at
      updated_at
      updated_by
    }
  }
`
