import gql from 'graphql-tag'

export const delete_room_topic_by_pk = gql`
  mutation delete_room_topic_by_pk($id: uuid!) {
    delete_room_topic_by_pk(id: $id) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      room_id
      topic
      updated_at
      updated_by
    }
  }
`
