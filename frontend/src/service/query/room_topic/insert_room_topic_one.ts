import gql from 'graphql-tag'

export const insert_room_topic_one = gql`
  mutation insert_room_topic_one($object: room_topic_insert_input!) {
    insert_room_topic_one(object: $object) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      room_id
      topic
      updated_at
      updated_by
    }
  }
`
