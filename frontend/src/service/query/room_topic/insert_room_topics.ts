import gql from 'graphql-tag'

export const insert_room_topics = gql`
  mutation insert_room_topics($objects: [room_topic_insert_input!]!) {
    insert_room_topic(objects: $objects) {
      returning {
        created_at
        created_by
        deleted_at
        deleted_by
        id
        room_id
        topic
        updated_at
        updated_by
      }
    }
  }
`
