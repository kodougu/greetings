import gql from 'graphql-tag'

export const insert_account_one = gql`
  mutation insert_account_one($object: account_insert_input!) {
    insert_account_one(object: $object) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      nickname
      updated_at
      updated_by
    }
  }
`
