import gql from 'graphql-tag'

export const account_by_pk = gql`
  query account_by_pk($id: uuid!) {
    account_by_pk(id: $id) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      nickname
      updated_at
      updated_by
    }
  }
`
