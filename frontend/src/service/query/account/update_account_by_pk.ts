import gql from 'graphql-tag'

export const insert_account_one = gql`
  mutation update_account_by_pk($id: uuid!, $_set: account_set_input!) {
    update_account_by_pk(_set: $_set, pk_columns: { id: $id }) {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      nickname
      updated_at
      updated_by
    }
  }
`
