import gql from 'graphql-tag'

export const insert_room_one = gql`
  mutation insert_participant_one($object: participant_insert_input!) {
    insert_participant_one(object: $object) {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      id
      role
      room_id
      updated_at
      updated_by
    }
  }
`
