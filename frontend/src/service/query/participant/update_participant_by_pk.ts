import gql from 'graphql-tag'

export const update_participant_by_pk = gql`
  mutation update_participant_by_pk(
    $id: uuid!
    $_set: participant_set_input!
  ) {
    update_participant_by_pk(pk_columns: { id: $id }, _set: $_set) {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      id
      role
      room_id
      updated_at
      updated_by
    }
  }
`
