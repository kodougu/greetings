import gql from 'graphql-tag'

export const delete_participant_by_pk = gql`
  mutation delete_participant_by_pk($id: uuid!) {
    delete_participant_by_pk(id: $id) {
      account_id
      created_at
      created_by
      deleted_at
      deleted_by
      id
      role
      room_id
      updated_at
      updated_by
    }
  }
`
