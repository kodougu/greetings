import gql from 'graphql-tag'

export const topic_all = gql`
  query topic_all {
    topic {
      created_at
      created_by
      deleted_at
      deleted_by
      id
      topic
      updated_at
      updated_by
    }
  }
`
