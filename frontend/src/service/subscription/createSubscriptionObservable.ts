import { DocumentNode, execute } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { SubscriptionClient } from 'subscriptions-transport-ws'

function convertWsEndpoint(graphqlEndpoint: string) {
  if (graphqlEndpoint.startsWith('https://')) {
    return `wss://${graphqlEndpoint.substring('https://'.length)}`
  } else if (graphqlEndpoint.startsWith('http://')) {
    return `ws://${graphqlEndpoint.substring('http://'.length)}`
  } else {
    throw new Error(`graphqlEndpoint is invalid. ${graphqlEndpoint}`)
  }
}

function getWsClient(wsEndpoint: string) {
  const client = new SubscriptionClient(wsEndpoint, {
    reconnect: true,
  })
  return client
}

export function createSubscriptionObservable(
  graphqlEndpoint: string,
  query: DocumentNode,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  variables: Record<string, any>,
) {
  const link = new WebSocketLink(
    getWsClient(convertWsEndpoint(graphqlEndpoint)),
  )
  return execute(link, { query: query, variables: variables })
}
