import { CONFIG } from '@/config'
import { createSubscriptionObservable } from './createSubscriptionObservable'
import { subscribe_room_by_pk as gql_subscribe_room_by_pk } from '@/service/query/room/subscribe_room_by_pk'
import { RoomModel, STATUS_OF_ROOM } from '../model/RoomModel'
import { Room, Subscribe_Room_By_PkSubscription } from '../generate/graphql'
import { DIALOG_IMAGE, roomStore } from '@/store/roomStore'
import { logger } from '@/util/logger'
import { playSound } from '@/util/sound'

export async function subscribe_room_by_pk(id: string) {
  const subscriptionClient = createSubscriptionObservable(
    CONFIG.VUE_APP_GRAPHQL_ENDPOINT, // GraphQL endpoint
    gql_subscribe_room_by_pk, // Subscription query
    { id }, // Query variables
  )
  const consumer = subscriptionClient.subscribe(
    async(eventData) => {
      // Do something on receipt of the event
      // logger.debug('Received event: ')
      // logger.debug(JSON.stringify(eventData, null, 2))

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const result: Subscribe_Room_By_PkSubscription = eventData.data as any
      if (result.room_by_pk) {
        const room: RoomModel = new RoomModel(result.room_by_pk as Room)
        roomStore.setRoom(room)
        if (roomStore.isMember) {
          if (room.status === STATUS_OF_ROOM.CHOOSE_TARGET_START) {
            roomStore.setDialogImage(DIALOG_IMAGE.CHOOSE)
            roomStore.setDiceDialog(true)
            await playSound('/sound/drumroll.mp3')
            await playSound('/sound/decision.mp3')
            roomStore.setDiceDialog(false)
            if (roomStore.isOwner) {
              await roomStore.changeStatus(STATUS_OF_ROOM.CHOOSE_TARGET_END)
            }
          } else if (room.status === STATUS_OF_ROOM.CHOOSE_TOPIC_START) {
            roomStore.setDialogImage(DIALOG_IMAGE.DICE)
            roomStore.setDiceDialog(true)
            await playSound('/sound/dice.mp3')
            await roomStore.setDiceDialog(false)
            if (roomStore.isSpeaker) {
              await roomStore.changeStatus(STATUS_OF_ROOM.CHOOSE_TOPIC_END)
            }
          }
        }
      }
    },
    err => {
      logger.error('Err')
      logger.error(err)
    },
  )
  logger.info(`Subscription Client connected. closed: ${consumer.closed}`)
}
