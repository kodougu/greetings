import { Talk } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'
import { AccountModel } from './AccountModel'
import { RoomTopicModel } from './RoomTopicModel'

export class TalkModel extends AbstractModel {
  room_id = ''
  account_id = ''
  account = new AccountModel()
  room_topic_id?: string | undefined = undefined
  room_topic?: RoomTopicModel | undefined = undefined

  started_at = ''
  finished_at = ''

  public constructor(plainObject?: Partial<TalkModel | Talk>) {
    super()
    Object.assign(this, plainObject)
    if (plainObject?.room_topic) {
      this.room_topic = new RoomTopicModel(plainObject?.room_topic)
    }
    if (plainObject?.account) {
      this.account = new AccountModel(plainObject?.account)
    }
  }
}
