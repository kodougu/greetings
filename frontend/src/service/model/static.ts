/**
 * 遠い未来の日時を表す定数。
 * updated_at や deleted_at の初期状態としてセットする。
 * deleted_at は将来的に「削除予約」を行うかもしれないので、いったん遠い未来をセットしている。
 * deleted_at が現在時刻より過去のものが削除されたものと判定する。
 */
export const FUTURE_TIME = '9999-12-31T00:00:00.000Z'
