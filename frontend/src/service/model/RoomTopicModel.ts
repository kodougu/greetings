import { Room_Topic } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'

export class RoomTopicModel extends AbstractModel {
  topic = ''

  public constructor(plainObject?: Partial<RoomTopicModel | Room_Topic>) {
    super()
    Object.assign(this, plainObject)
  }
}
