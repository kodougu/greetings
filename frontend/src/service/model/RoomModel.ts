import { Participant, Room, Room_Topic, Talk } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'
import { ParticipantModel } from './ParticipantModel'
import { RoomTopicModel } from './RoomTopicModel'
import { TalkModel } from './TalkModel'

export const STATUS_OF_ROOM = {
  CREATED: 1,
  CHOOSE_TARGET_START: 10,
  CHOOSE_TARGET_END: 15,
  CHOOSE_TOPIC_START: 20,
  CHOOSE_TOPIC_END: 25,
  FINISH_TALK: 30,
} as const
export type TYPE_STATUS_OF_ROOM = typeof STATUS_OF_ROOM[keyof typeof STATUS_OF_ROOM]

export class RoomModel extends AbstractModel {
  title = ''
  talk_time = 5

  participants: ParticipantModel[] = []
  room_topics: RoomTopicModel[] = []
  talks: TalkModel[] = []

  current_talk_id = ''
  talk = new TalkModel()
  status = 1

  public constructor(plainObject?: Partial<RoomModel | Room>) {
    super()
    Object.assign(this, plainObject)
    if (plainObject?.participants) {
      this.participants = (plainObject.participants as Array<
        ParticipantModel | Participant
      >).map(item => new ParticipantModel(item))
    }
    if (plainObject?.room_topics) {
      const room_topics = (plainObject.room_topics as Array<
        RoomTopicModel | Room_Topic
      >).map(item => new RoomTopicModel(item))
      this.room_topics = room_topics.filter(roomTopic => {
        if (roomTopic.deleted_by) {
          return false
        }
        return true
      })
      console.log(this.room_topics)
    }
    if (plainObject?.talks) {
      this.talks = (plainObject.talks as Array<TalkModel | Talk>).map(
        item => new TalkModel(item),
      )
    }
    if (plainObject?.talk) {
      this.talk = new TalkModel(plainObject?.talk)
    }
  }
}
