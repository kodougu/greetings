import { Topic } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'

export class TopicModel extends AbstractModel {
  topic = ''

  public constructor(plainObject?: Partial<TopicModel | Topic>) {
    super()
    Object.assign(this, plainObject)
  }
}
