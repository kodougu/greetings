import { FUTURE_TIME } from './static'

export abstract class AbstractModel {
  _typename = ''
  id = ''
  created_at = ''
  created_by = ''
  updated_at = FUTURE_TIME
  updated_by = ''
  deleted_at = FUTURE_TIME
  deleted_by = ''
}
