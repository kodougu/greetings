import { Account } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'

export class AccountModel extends AbstractModel {
  nickname = ''
  public constructor(
    plainObject?: Partial<AccountModel | Account>,
  ) {
    super()
    Object.assign(this, plainObject)
  }
}
