import { Participant } from '../generate/graphql'
import { AbstractModel } from './AbstractModel'
import { AccountModel } from './AccountModel'

export const ROLE_OF_ROOM = {
  OWNER: 'OWNER',
  USER: 'USER',
} as const
export type TYPE_ROLE_OF_ROOM = typeof ROLE_OF_ROOM[keyof typeof ROLE_OF_ROOM]

export class ParticipantModel extends AbstractModel {
  account_id = ''
  role: TYPE_ROLE_OF_ROOM = 'USER'
  room_id = ''
  account = new AccountModel()

  public constructor(plainObject?: Partial<ParticipantModel | Participant>) {
    super()
    Object.assign(this, plainObject)
    if (plainObject?.account) {
      this.account = new AccountModel(plainObject?.account)
    }
  }
}
