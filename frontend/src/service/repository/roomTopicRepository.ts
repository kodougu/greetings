import { RoomTopicModel } from '../model/RoomTopicModel'
import { getSdk } from './getGraphQLClient'

export async function insert_room_topics(
  roomTopics: RoomTopicModel[],
): Promise<RoomTopicModel[]> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const objects = roomTopics.map(roomTopic => {
    const {
      id,
      created_at,
      updated_at,
      deleted_at,
      updated_by,
      deleted_by,
      _typename,
      ...object
    } = roomTopic
    return object
  })
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.insert_room_topics({ objects })
  if (result.insert_room_topic) {
    return result.insert_room_topic.returning.map(
      item => new RoomTopicModel(item),
    )
  } else {
    return []
  }
}

export async function insert_room_topic_one(
  roomTopic: RoomTopicModel,
): Promise<RoomTopicModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    id,
    created_at,
    updated_at,
    deleted_at,
    updated_by,
    deleted_by,
    _typename,
    ...object
  } = roomTopic
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.insert_room_topic_one({ object })
  if (result.insert_room_topic_one) {
    return new RoomTopicModel(result.insert_room_topic_one)
  } else {
    return undefined
  }
}

export async function update_room_topic_by_pk(
  roomTopicId: string,
  roomTopic: RoomTopicModel,
): Promise<RoomTopicModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    id,
    created_at,
    deleted_at,
    created_by,
    deleted_by,
    _typename,
    ...object
  } = roomTopic
  /* eslint-enable @typescript-eslint/no-unused-vars */

  const result = await sdk.update_room_topic_by_pk({
    id: roomTopicId,
    _set: object,
  })
  if (result.update_room_topic_by_pk) {
    return new RoomTopicModel(result.update_room_topic_by_pk)
  } else {
    return undefined
  }
}

export async function logical_delete_room_topic_by_pk(
  roomTopicId: string,
  roomTopic: RoomTopicModel,
): Promise<RoomTopicModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    id,
    created_at,
    updated_at,
    created_by,
    updated_by,
    _typename,
    ...object
  } = roomTopic
  /* eslint-enable @typescript-eslint/no-unused-vars */

  const result = await sdk.update_room_topic_by_pk({
    id: roomTopicId,
    _set: object,
  })
  if (result.update_room_topic_by_pk) {
    return new RoomTopicModel(result.update_room_topic_by_pk)
  } else {
    return undefined
  }
}
