import { Account_Insert_Input, Account_Set_Input } from '../generate/graphql'
import { getSdk } from './getGraphQLClient'

export async function account_by_pk(id: string) {
  const sdk = getSdk()
  const result = await sdk.account_by_pk({ id })
  if (result.account_by_pk) {
    return result.account_by_pk
  } else {
    return undefined
  }
}

export async function insert_account_one(object: Account_Insert_Input) {
  const sdk = getSdk()
  const result = await sdk.insert_account_one({ object })
  if (result.insert_account_one) {
    return result.insert_account_one
  } else {
    return undefined
  }
}

export async function update_account_by_pk(
  id: string,
  _set: Account_Set_Input,
) {
  const sdk = getSdk()
  const result = await sdk.update_account_by_pk({ id, _set })
  if (result.update_account_by_pk) {
    return result.update_account_by_pk
  } else {
    return undefined
  }
}
