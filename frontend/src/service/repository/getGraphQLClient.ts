import { CONFIG } from '@/config'
import { GraphQLClient } from 'graphql-request'
import { getSdk as GetSDK, Sdk } from '@/service/generate/graphql'
import { logger } from '@/util/logger'

function setExtendHeadersForAdminClient(
  client: GraphQLClient,
  extendHeaders?: Map<string, string>,
): void {
  if (extendHeaders) {
    extendHeaders.forEach((value, key) => {
      if (key !== 'x-hasura-role' && key !== 'x-hasura-admin-secret') {
        client.setHeader(key, value)
      }
    })
  }
}

/**
 * admin権限で動作するiのgraphQLClientを生成する関数
 * extendHeadersでは、roleを指定できないようにしている
 */
function getGraphQLClient(extendHeaders?: Map<string, string>) {
  logger.debug(CONFIG.VUE_APP_GRAPHQL_ENDPOINT)
  const client = new GraphQLClient(CONFIG.VUE_APP_GRAPHQL_ENDPOINT)
  setExtendHeadersForAdminClient(client, extendHeaders)
  return client
}
export function getSdk(extendHeaders?: Map<string, string>): Sdk {
  const client = getGraphQLClient(extendHeaders)
  return GetSDK(client)
}
