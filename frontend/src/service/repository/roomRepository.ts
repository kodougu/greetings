import { RoomModel } from '@/service/model/RoomModel'
import { Room } from '../generate/graphql'
import { getSdk } from './getGraphQLClient'

export async function room_by_pk(id: string): Promise<RoomModel | undefined> {
  const sdk = getSdk()
  const result = await sdk.room_by_pk({ id })
  if (result.room_by_pk) {
    return new RoomModel(result.room_by_pk as Room)
  } else {
    return undefined
  }
}

export async function insert_room_one(
  room: RoomModel,
): Promise<RoomModel | undefined> {
  const sdk = getSdk()
  // room のオブジェクトから GraphQL の insert に投げる形でデータを取得
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    participants,
    room_topics,
    talks,
    current_talk_id,
    talk,
    id,
    created_at,
    updated_at,
    deleted_at,
    updated_by,
    deleted_by,
    _typename,
    ...object
  } = room
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.insert_room_one({ object })
  if (result.insert_room_one) {
    return new RoomModel(result.insert_room_one)
  } else {
    return undefined
  }
}

export async function update_room_by_pk(
  roomId: string,
  room: RoomModel,
): Promise<RoomModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    participants,
    room_topics,
    talks,
    talk,
    id,
    created_at,
    deleted_at,
    created_by,
    deleted_by,
    _typename,
    ...object
  } = room
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.update_room_by_pk({ id: roomId, _set: object })
  if (result.update_room_by_pk) {
    return new RoomModel(result.update_room_by_pk)
  } else {
    return undefined
  }
}

export async function createRoom(account_id: string): Promise<RoomModel> {
  const room = await insert_room_one(
    new RoomModel({
      created_by: account_id,
    }),
  )
  if (!room) {
    throw new Error("service can't create the room. please try again.")
  }
  return room
}
