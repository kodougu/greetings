import { TalkModel } from '../model/TalkModel'
import { getSdk } from './getGraphQLClient'

export async function insert_talk_one(
  talk: TalkModel,
): Promise<TalkModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    room_topic,
    account,
    started_at,
    finished_at,
    id,
    created_at,
    updated_at,
    deleted_at,
    updated_by,
    deleted_by,
    _typename,
    ...object
  } = talk
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.insert_talk_one({ object })
  if (result.insert_talk_one) {
    return new TalkModel(result.insert_talk_one)
  } else {
    return undefined
  }
}

export async function update_talk_by_pk(
  talkId: string,
  talk: TalkModel,
): Promise<TalkModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    room_topic,
    account,
    id,
    created_at,
    deleted_at,
    created_by,
    deleted_by,
    _typename,
    ...object
  } = talk
  /* eslint-enable @typescript-eslint/no-unused-vars */

  const result = await sdk.update_talk_by_pk({
    id: talkId,
    _set: object,
  })
  if (result.update_talk_by_pk) {
    return new TalkModel(result.update_talk_by_pk)
  } else {
    return undefined
  }
}
