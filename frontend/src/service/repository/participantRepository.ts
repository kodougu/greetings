import { ParticipantModel } from '../model/ParticipantModel'
import { getSdk } from './getGraphQLClient'

export async function insert_participant_one(
  participant: ParticipantModel,
): Promise<ParticipantModel | undefined> {
  const sdk = getSdk()
  // room のオブジェクトから GraphQL の insert に投げる形でデータを取得（セット出来ないプロパティを削除）
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    id,
    created_at,
    updated_at,
    deleted_at,
    updated_by,
    deleted_by,
    _typename,
    account,
    ...object
  } = participant
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.insert_participant_one({ object })
  if (result.insert_participant_one) {
    return new ParticipantModel(result.insert_participant_one)
  } else {
    return undefined
  }
}

export async function update_participant_by_pk(
  participantId: string,
  participant: ParticipantModel,
): Promise<ParticipantModel | undefined> {
  const sdk = getSdk()
  /* eslint-disable @typescript-eslint/no-unused-vars */
  const {
    id,
    created_at,
    deleted_at,
    created_by,
    deleted_by,
    _typename,
    ...object
  } = participant
  /* eslint-enable @typescript-eslint/no-unused-vars */
  const result = await sdk.update_participant_by_pk({
    id: participantId,
    _set: object,
  })
  if (result.update_participant_by_pk) {
    return new ParticipantModel(result.update_participant_by_pk)
  } else {
    return undefined
  }
}

export async function delete_participant_by_pk(
  participantId: string,
): Promise<ParticipantModel | undefined> {
  const sdk = getSdk()
  const result = await sdk.delete_participant_by_pk({ id: participantId })
  if (result.delete_participant_by_pk) {
    return new ParticipantModel(result.delete_participant_by_pk)
  } else {
    return undefined
  }
}
