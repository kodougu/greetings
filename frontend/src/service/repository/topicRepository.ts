import { TopicModel } from '../model/TopicModel'
import { getSdk } from './getGraphQLClient'

export async function topic_all(): Promise<TopicModel[]> {
  const sdk = getSdk()
  const result = await sdk.topic_all()
  if (result.topic) {
    return result.topic.map(topic => new TopicModel(topic))
  } else {
    return []
  }
}
