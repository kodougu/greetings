import { Resources } from './resources'
import { LocaleMessageObject, LocaleMessageArray } from 'vue-i18n'

export class JapaneseResources implements Resources {
  [key: string]: string | LocaleMessageObject | LocaleMessageArray
  title = 'Greetings'
  createRoom = '雑談ルームを作成'
}
