import { Resources } from './resources'
import { LocaleMessageObject, LocaleMessageArray } from 'vue-i18n'

export class EnglishResources implements Resources {
  [key: string]: string | LocaleMessageObject | LocaleMessageArray
  title = 'Greetings'
  createRoom = 'Create a room'
}
