import { LocaleMessageObject, LocaleMessageArray } from 'vue-i18n'

export interface Resources extends LocaleMessageObject {
  [key: string]: string | LocaleMessageObject | LocaleMessageArray;
  title: string;
  createRoom: string;
}
