import Vue from 'vue'
import VueI18n, { LocaleMessages } from 'vue-i18n'

import { EnglishResources } from './en'
import { JapaneseResources } from './ja'
import { Resources } from './resources'

export type NestedPath<T> = {
  [key in keyof T]: T[key]
}
Vue.use(VueI18n)

const en = new EnglishResources()
const ja = new JapaneseResources()

const messages: LocaleMessages = {
  en,
  ja,
}

export function getLanguage (): string {
  if (window.localStorage) {
    const savedLanguage = window.localStorage.getItem('language')
    if (savedLanguage) {
      return savedLanguage
    }
  }
  let language = 'en'
  if (window.navigator) {
    const navigator = window.navigator
    if (navigator.languages && navigator.languages[0]) {
      language = navigator.languages[0]
    } else if (navigator.language) {
      language = navigator.language
    }
  }
  return language
}

const locale = getLanguage()
const dateTimeFormats = {
  'en-US': {
    short: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
  },
}
const i18n = new VueI18n({
  locale,
  messages,
  dateTimeFormats,
})

export function setLanguage (language: string): void {
  if (window.localStorage) {
    window.localStorage.setItem('language', language)
  }
  i18n.locale = language
}

export function generatePathMap<T extends Resources> (obj: T, basePath = '') {
  return Object.keys(obj).reduce((result: NestedPath<Resources>, key) => {
    const path = basePath === '' ? key : `${basePath}.${key}`
    if (typeof obj[key] === 'object') {
      result[key] = generatePathMap(obj[key] as T, path)
    } else {
      result[key] = path
    }
    return result
  }, {} as NestedPath<Resources>)
}

const i18nHints: NestedPath<Resources> = generatePathMap(en)

export { i18n, i18nHints }
