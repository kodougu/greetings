/* eslint import/first: 0 */
import dotenv from 'dotenv'
dotenv.config()

import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import { store } from './store'
import { i18n } from '@/i18n'
import vuetify from './plugins/vuetify'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App),
}).$mount('#app')
