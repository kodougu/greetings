import { config } from 'dotenv'
config()

const VUE_APP_GRAPHQL_ENDPOINT = process.env.VUE_APP_GRAPHQL_ENDPOINT || ''

export const CONFIG = {
  VUE_APP_GRAPHQL_ENDPOINT,
}
