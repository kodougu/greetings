import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule,
} from 'vuex-module-decorators'
import { store } from '@/store'
import {
  account_by_pk,
  insert_account_one,
  update_account_by_pk,
} from '@/service/repository/accountRepository'

@Module({ dynamic: true, store, name: 'account', namespaced: true })
class AccountStore extends VuexModule {
  private _openDialog = false
  private _id = ''
  private _nickname = ''
  public get openDialog() {
    return this._openDialog
  }

  public get id() {
    return this._id
  }

  public get nickname() {
    return this._nickname
  }

  @Mutation
  public setOpenDialog(openDialog: boolean) {
    this._openDialog = openDialog
  }

  @Mutation
  private setId(id: string) {
    this._id = id
  }

  @Mutation
  private setNickname(nickname: string) {
    this._nickname = nickname
  }

  @Action({ rawError: true })
  public async readSavedAccount() {
    if (this.id) {
      return
    }
    if (window.localStorage) {
      const id = window.localStorage.getItem('id')
      if (id) {
        // GraphQLでNickName取得
        const account = await account_by_pk(id)
        if (account && account.nickname) {
          this.setNickname(account.nickname)
          this.setId(id)
          return
        }
      }
    }
    // idを発行して保持
    const account = await insert_account_one({})
    if (window.localStorage) {
      window.localStorage.setItem('id', account?.id)
    }
    this.setId(account?.id)
  }

  @Action({ rawError: true })
  public async saveNickname(nickname: string) {
    await update_account_by_pk(this.id, { nickname })
    this.setNickname(nickname)
  }
}

export const accountStore = getModule(AccountStore)
