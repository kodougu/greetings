import {
  VuexModule,
  Module,
  Mutation,
  Action,
  getModule,
} from 'vuex-module-decorators'
import { store } from '@/store'
import { router } from '@/router'
import { logger } from '@/util/logger'
import { accountStore } from './accountStore'
import {
  createRoom,
  room_by_pk,
  update_room_by_pk,
} from '@/service/repository/roomRepository'
import {
  RoomModel,
  STATUS_OF_ROOM,
  TYPE_STATUS_OF_ROOM,
} from '@/service/model/RoomModel'
import { subscribe_room_by_pk } from '@/service/subscription/roomSubscription'
import { insert_participant_one } from '@/service/repository/participantRepository'
import {
  ParticipantModel,
  ROLE_OF_ROOM,
} from '@/service/model/ParticipantModel'
import { topic_all } from '@/service/repository/topicRepository'
import { randomElement, randomElements } from '@/util/random'
import { RoomTopicModel } from '@/service/model/RoomTopicModel'
import {
  insert_room_topics,
  logical_delete_room_topic_by_pk,
} from '@/service/repository/roomTopicRepository'
import { TopicModel } from '@/service/model/TopicModel'
import {
  insert_talk_one,
  update_talk_by_pk,
} from '@/service/repository/talkRepository'
import { TalkModel } from '@/service/model/TalkModel'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import { mapSeries } from 'bluebird'
dayjs.extend(utc)

export const DIALOG_IMAGE = {
  DICE: 'DICE',
  CHOOSE: 'CHOOSE',
} as const
export type TYPE_DIALOG_IMAGE = typeof DIALOG_IMAGE[keyof typeof DIALOG_IMAGE]

@Module({ dynamic: true, store, name: 'room', namespaced: true })
class RoomStore extends VuexModule {
  private _inviteDialog = false
  private _diceDialog = false
  private _room = new RoomModel()
  private _dialogImage: TYPE_DIALOG_IMAGE = DIALOG_IMAGE.DICE

  get inviteDialog() {
    return this._inviteDialog
  }

  get diceDialog() {
    return this._diceDialog
  }

  get room() {
    return this._room
  }

  get dialogImage() {
    return this._dialogImage
  }

  get isOwner() {
    let owner = false
    if (this._room) {
      this._room.participants.forEach(participant => {
        if (
          participant.role === ROLE_OF_ROOM.OWNER &&
          accountStore.id === participant.account_id
        ) {
          owner = true
        }
      })
    }
    return owner
  }

  get isMember() {
    const account_id = accountStore.id
    if (account_id) {
      let exist = false
      this.room.participants.forEach(participant => {
        if (account_id === participant.account_id) exist = true
      })
      return exist
    }
  }

  get isSpeaker(): boolean {
    const accountId = accountStore.id
    if (this.room && this.room.talk?.account_id === accountId) {
      return true
    }
    return false
  }

  @Mutation
  setInviteDialog(inviteDialog: boolean) {
    this._inviteDialog = inviteDialog
  }

  @Mutation
  setDiceDialog(diceDialog: boolean) {
    this._diceDialog = diceDialog
  }

  @Mutation
  setRoom(room: RoomModel) {
    this._room = room
  }

  @Mutation
  setDialogImage(dialogImage: TYPE_DIALOG_IMAGE) {
    this._dialogImage = dialogImage
  }

  @Action({ rawError: true })
  public async createRoomAndMovePage() {
    logger.debug('createRoomAndMovePage')
    // アカウント情報の初期化
    await accountStore.readSavedAccount()
    const account_id = accountStore.id
    // Roomの作成
    const room = await createRoom(account_id)
    // 作成者をルームの参加者（ルームのオーナー）にする
    await insert_participant_one(
      new ParticipantModel({
        account_id,
        room_id: room.id,
        role: ROLE_OF_ROOM.OWNER,
        created_by: account_id,
      }),
    )
    // ルームの話題をランダムで6件登録する
    const topics = await topic_all()
    const roomTopics = randomElements<TopicModel>(topics, 6).map(
      item =>
        new RoomTopicModel({
          room_id: room.id,
          topic: item.topic,
        }),
    )
    logger.i('RoomTopics', JSON.stringify(roomTopics))
    await insert_room_topics(roomTopics)
    this.setRoom(room)
    router.push(`/room/${room?.id}`)
  }

  @Action({ rawError: true })
  public async loadRoom(id: string) {
    logger.debug(`loadRoom. id: ${id}`)
    // Roomの情報を取得
    const room = await room_by_pk(id)
    if (!room) {
      // TODO エラー
      router.push('/404')
    } else {
      this.setRoom(room)
      // アカウント情報の初期化
      await accountStore.readSavedAccount()
      // Room情報の変更検知
      subscribe_room_by_pk(id)
      // ニックネームが決まってなかったら決める
      if (!accountStore.nickname) {
        accountStore.setOpenDialog(true)
      }
    }
  }

  @Action({ rawError: true })
  public async joinRoom() {
    if (!this.isMember) {
      const account_id = accountStore.id
      await insert_participant_one(
        new ParticipantModel({
          account_id,
          room_id: this.room.id,
          role: ROLE_OF_ROOM.USER,
          created_by: account_id,
        }),
      )
    }
  }

  @Action({ rawError: true })
  async saveTitle(title: string) {
    if (this._room) {
      // logger.debug(`update_room_by_pk. id: ${this.room.id}`)
      await update_room_by_pk(
        this._room.id,
        new RoomModel({ ...this._room, title }),
      )
    }
  }

  @Action({ rawError: true })
  async saveTalkTime(talk_time: number) {
    if (this._room) {
      // logger.debug(`update_room_by_pk. id: ${this.room.id}`)
      await update_room_by_pk(
        this._room.id,
        new RoomModel({ ...this._room, talk_time }),
      )
    }
  }

  @Action({ rawError: true })
  async chooseSpeaker() {
    const targets = this.room.participants.filter(participant => {
      let alreadyTalk = false
      this.room.talks.forEach(talk => {
        if (talk.account_id === participant.account_id) {
          alreadyTalk = true
        }
      })
      return !alreadyTalk
    })
    logger.debug(targets)
    if (!targets || targets.length === 0) {
      return false
    }
    const target = randomElement(targets)
    // talk作成
    if (target && target.account_id) {
      const talkObj = new TalkModel({
        created_by: accountStore.id,
        room_id: this.room.id,
        account_id: target?.account_id,
      })
      const talk = await insert_talk_one(talkObj)
      logger.info(talk)
      // current_talk更新
      await update_room_by_pk(
        this.room.id,
        new RoomModel({
          ...this.room,
          updated_by: accountStore.id,
          current_talk_id: talk?.id,
          status: STATUS_OF_ROOM.CHOOSE_TARGET_START,
        }),
      )
      return true
    }
    return false
  }

  @Action({ rawError: true })
  async chooseTopic() {
    const topic = randomElement(this.room.room_topics)
    // room_topicをセット
    await update_talk_by_pk(
      this.room.talk.id,
      new TalkModel({
        ...this.room.talk,
        room_topic_id: topic.id,
        started_at: dayjs().format(),
      }),
    )
    await update_room_by_pk(
      this.room.id,
      new RoomModel({
        ...this.room,
        updated_by: accountStore.id,
        status: STATUS_OF_ROOM.CHOOSE_TOPIC_START,
      }),
    )
  }

  @Action({ rawError: true })
  async finishTalk() {
    await update_talk_by_pk(
      this.room.talk.id,
      new TalkModel({
        ...this.room.talk,
        finished_at: dayjs().format(),
      }),
    )
    await update_room_by_pk(
      this.room.id,
      new RoomModel({
        ...this.room,
        updated_by: accountStore.id,
        status: STATUS_OF_ROOM.FINISH_TALK,
      }),
    )
  }

  @Action({ rawError: true })
  async changeStatus(status: TYPE_STATUS_OF_ROOM) {
    await update_room_by_pk(
      this.room.id,
      new RoomModel({
        ...this.room,
        updated_by: accountStore.id,
        status: status,
      }),
    )
  }

  @Action({ rawError: true })
  async changeTopics() {
    // 今までのTopicは論理削除
    mapSeries(this.room.room_topics, async topic => {
      await logical_delete_room_topic_by_pk(
        topic.id,
        new RoomTopicModel({
          deleted_at: dayjs().format(),
          deleted_by: accountStore.id,
        }),
      )
    })
    // ルームの話題をランダムで6件登録する
    const topics = await topic_all()
    const roomTopics = randomElements<TopicModel>(topics, 6).map(
      item =>
        new RoomTopicModel({
          room_id: this.room.id,
          topic: item.topic,
        }),
    )
    logger.i('RoomTopics', JSON.stringify(roomTopics))
    await insert_room_topics(roomTopics)
  }
}

export const roomStore = getModule(RoomStore)
