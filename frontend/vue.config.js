module.exports = {
  lintOnSave: false,
  transpileDependencies: ['vuetify'],
  pages: {
    index: {
      entry: 'src/main.ts',
      title: 'ごきげんよう',
    },
  },
  devServer: {
    host: '0.0.0.0',
    disableHostCheck: true
  }
}
