#!/bin/bash -ex

export $(cat .env | grep -v ^# | xargs);

# Hasura起動
docker-compose up -d hasura

# Hasuraの起動待機
until docker-compose run --rm curl -i "http://hasura:8080/console" | grep "200 OK"
do
  echo "hasura is unavailable - sleeping"
  sleep 1
done

# Frontendを起動
frontend=frontend-build
frontend_port=8080

#frontend=frontend-nginx
#frontend_port=80

docker-compose up -d ${frontend}

# Frontendの起動待機
until docker-compose run --rm curl -i "http://${frontend}:${frontend_port}" | grep "200 OK"
do
  echo "frontend is unavailable - sleeping"
  sleep 1
done

docker-compose up -d https-portal

echo "service started"

docker-compose logs -f
