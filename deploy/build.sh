#!/bin/bash -ex

docker-compose down

export $(cat .env | grep -v ^# | xargs);

# DBを起動
docker-compose up -d postgres
sleep 1

# DBの起動待機
until docker-compose exec postgres sh -c "PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U postgres -c '\q'"
do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

# Hasura CLI でマイグレーションなどを適用(hasura/graphql-engine:v1.3.2.cli-migrations-v2 のイメージを起動すれば適用される)
docker-compose up -d hasura-cli
until docker-compose run --rm curl -i "http://hasura-cli:8080/console" | grep "200 OK"
do
  echo "hasura-cli is unavailable - sleeping"
  sleep 1
done
docker-compose exec hasura-cli hasura-cli seeds apply --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check

docker-compose stop hasura-cli
docker-compose rm -f hasura-cli

# Frontendをビルド
#docker-compose run --rm frontend-build yarn install
#docker-compose run --rm frontend-build yarn build
