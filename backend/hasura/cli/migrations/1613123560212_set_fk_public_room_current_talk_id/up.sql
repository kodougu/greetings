alter table "public"."room"
           add constraint "room_current_talk_id_fkey"
           foreign key ("current_talk_id")
           references "public"."talk"
           ("id") on update set null on delete set null;
