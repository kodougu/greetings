alter table "public"."room_topic"
           add constraint "room_topic_room_id_fkey"
           foreign key ("room_id")
           references "public"."room"
           ("id") on update cascade on delete cascade;
