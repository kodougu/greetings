alter table "public"."talk"
           add constraint "talk_room_id_fkey"
           foreign key ("room_id")
           references "public"."room"
           ("id") on update cascade on delete cascade;
