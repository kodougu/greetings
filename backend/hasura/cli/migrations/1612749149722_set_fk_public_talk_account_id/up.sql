alter table "public"."talk"
           add constraint "talk_account_id_fkey"
           foreign key ("account_id")
           references "public"."account"
           ("id") on update restrict on delete restrict;
