alter table "public"."participant" drop constraint "participant_room_id_fkey",
             add constraint "participant_room_id_fkey"
             foreign key ("room_id")
             references "public"."room"
             ("id") on update cascade on delete cascade;
