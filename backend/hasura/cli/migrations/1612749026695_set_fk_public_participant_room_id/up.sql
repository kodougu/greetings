alter table "public"."participant"
           add constraint "participant_room_id_fkey"
           foreign key ("room_id")
           references "public"."room"
           ("id") on update restrict on delete restrict;
