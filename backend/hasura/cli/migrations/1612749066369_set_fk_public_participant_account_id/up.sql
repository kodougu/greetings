alter table "public"."participant"
           add constraint "participant_account_id_fkey"
           foreign key ("account_id")
           references "public"."account"
           ("id") on update restrict on delete restrict;
