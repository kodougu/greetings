CREATE EXTENSION IF NOT EXISTS pgcrypto;
-- 雑談: talk
drop table if exists talk cascade;
create table talk (
  id uuid not null default gen_random_uuid(),
  room_id uuid not null,
  account_id uuid not null,
  room_topic_id uuid not null,
  started_at timestamp,
  finished_at timestamp,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint talk_PKC primary key (id)
);
-- アカウント: account
drop table if exists account cascade;
create table account (
  id uuid not null default gen_random_uuid(),
  nickname character varying(64) not null,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint account_PKC primary key (id)
);
-- ルームでの話題
drop table if exists room_topic cascade;
create table room_topic (
  id uuid not null default gen_random_uuid(),
  room_id uuid not null,
  topic character varying(256) not null,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint room_topic_PKC primary key (id)
);
-- デフォルトの話題: topic
drop table if exists topic cascade;
create table topic (
  id uuid not null default gen_random_uuid(),
  topic character varying(256) not null,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint topic_PKC primary key (id)
);
-- 参加者: participant
drop table if exists participant cascade;
create table participant (
  id uuid not null default gen_random_uuid(),
  room_id uuid not null,
  account_id uuid not null,
  role character varying(12) not null,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint participant_PKC primary key (id)
);
create unique index room_account_unique_index on participant(room_id, account_id);
-- 雑談ルーム: room
drop table if exists room cascade;
create table room (
  id uuid not null default gen_random_uuid(),
  title character varying(256),
  talk_time integer,
  created_by character varying(36),
  updated_by character varying(36),
  deleted_by character varying(36),
  created_at timestamp default now() not null,
  updated_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  deleted_at timestamp default '9999-12-31T00:00:00.000Z' not null,
  constraint room_PKC primary key (id)
);
comment on table talk is '雑談: talk';
comment on column talk.id is 'id';
comment on column talk.room_id is 'room_id';
comment on column talk.account_id is 'account_id';
comment on column talk.room_topic_id is 'room_topic_id';
comment on column talk.started_at is 'started_at';
comment on column talk.finished_at is 'finished_at';
comment on column talk.created_by is 'created_by';
comment on column talk.updated_by is 'updated_by';
comment on column talk.deleted_by is 'deleted_by';
comment on column talk.created_at is 'created_at';
comment on column talk.updated_at is 'updated_at';
comment on column talk.deleted_at is 'deleted_at';
comment on table account is 'アカウント: account';
comment on column account.id is 'id';
comment on column account.nickname is 'nickname';
comment on column account.created_by is 'created_by';
comment on column account.updated_by is 'updated_by';
comment on column account.deleted_by is 'deleted_by';
comment on column account.created_at is 'created_at';
comment on column account.updated_at is 'updated_at';
comment on column account.deleted_at is 'deleted_at';
comment on table room_topic is 'ルームでの話題';
comment on column room_topic.id is 'id';
comment on column room_topic.room_id is 'room_id';
comment on column room_topic.topic is 'topic';
comment on column room_topic.created_by is 'created_by';
comment on column room_topic.updated_by is 'updated_by';
comment on column room_topic.deleted_by is 'deleted_by';
comment on column room_topic.created_at is 'created_at';
comment on column room_topic.updated_at is 'updated_at';
comment on column room_topic.deleted_at is 'deleted_at';
comment on table topic is 'デフォルトの話題: topic';
comment on column topic.id is 'id';
comment on column topic.topic is 'topic';
comment on column topic.created_by is 'created_by';
comment on column topic.updated_by is 'updated_by';
comment on column topic.deleted_by is 'deleted_by';
comment on column topic.created_at is 'created_at';
comment on column topic.updated_at is 'updated_at';
comment on column topic.deleted_at is 'deleted_at';
comment on table participant is '参加者: participant';
comment on column participant.id is 'id';
comment on column participant.room_id is 'room_id';
comment on column participant.account_id is 'account_id';
comment on column participant.role is 'role';
comment on column participant.created_by is 'created_by';
comment on column participant.updated_by is 'updated_by';
comment on column participant.deleted_by is 'deleted_by';
comment on column participant.created_at is 'created_at';
comment on column participant.updated_at is 'updated_at';
comment on column participant.deleted_at is 'deleted_at';
comment on table room is '雑談ルーム: room';
comment on column room.id is 'id';
comment on column room.title is 'title';
comment on column room.talk_time is 'talk_time';
comment on column room.created_by is 'created_by';
comment on column room.updated_by is 'updated_by';
comment on column room.deleted_by is 'deleted_by';
comment on column room.created_at is 'created_at';
comment on column room.updated_at is 'updated_at';
comment on column room.deleted_at is 'deleted_at';