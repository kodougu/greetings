drop table if exists talk cascade;
drop table if exists account cascade;
drop table if exists room_topic cascade;
drop table if exists topic cascade;
drop table if exists participant cascade;
drop table if exists room cascade;
