alter table "public"."talk"
           add constraint "talk_room_topic_id_fkey"
           foreign key ("room_topic_id")
           references "public"."room_topic"
           ("id") on update restrict on delete restrict;
