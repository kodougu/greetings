#!/bin/bash -ex

docker-compose down

export $(cat .env | grep -v ^# | xargs);

# Hasuraサーバーにアクセスできるホスト名を入力してください（疎通確認に利用）host.docker.internalやlocalhost
BACKEND_SERVER=localhost

# DBを起動
docker-compose up -d postgres
sleep 1

# DBの起動待機
until docker-compose exec postgres sh -c "PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U postgres -c '\q'"
do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done


# Hasura起動
docker-compose up -d hasura

# Hasuraの起動待機
until curl -i "http://${BACKEND_SERVER}:8080/console" | grep "200 OK"
do
  echo "hasura is unavailable - sleeping"
  sleep 1
done

# Hasura CLI でマイグレーションなどを適用
# Hasura CLIはネイティブにインストールする必要あり(グローバルインストールではなく、ディレクトリにインストールしている)
cd ../backend/hasura/
yarn install
# yarn hasura init
# yarn hasura metadata export --project cli --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check
yarn hasura migrate apply --project cli --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check
yarn hasura metadata apply --project cli --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check
yarn hasura seeds apply --project cli --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check
yarn hasura console --project cli --address 0.0.0.0 --console-port 9695 --api-port 9696 --no-browser --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check
