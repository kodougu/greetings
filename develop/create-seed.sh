#!/bin/bash -ex

export $(cat .env | grep -v ^# | xargs);

cd ../backend/hasura

yarn hasura seeds create topics --project cli --from-table topic --admin-secret ${HASURA_GRAPHQL_ADMIN_SECRET} --skip-update-check

